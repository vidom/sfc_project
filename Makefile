# SFC 2017/2018 Demonstration of Convolutional Neural Network activity
# Author: Matej Vido, xvidom00@stud.fit.vutbr.cz
# Date:   2017/11

.PHONY: src doc clean purge pack

src:
	make -C src

doc:
	make -C doc

clean:
	make clean -C src
	make clean -C doc

purge:
	make purge -C src
	make purge -C doc

pack:
