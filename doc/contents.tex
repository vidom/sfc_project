%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Úvod}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Táto práca popisuje výsledok projektu, ktorého cieľom bola implementácia
aplikácie pre demonštráciu činnosti konvolučnej neurónovej siete.
Sekcia~\ref{sec:cnn} poskytuje teoretický základ o konvolučných neurónových sieťach.
V sekcii~\ref{sec:implementation} je popísaná architektúra a implementácia vytvorenej
aplikácie.
Sekcia~\ref{sec:user_manual} obsahuje užívateľskú príručku a popisuje použitie aplikácie.
V závere (sekcia~\ref{sec:conclusion}) sú zhrnuté výsledky.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Konvolučné neurónové siete}\label{sec:cnn}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Konvolučné neurónové siete sú dopredné neurónové siete, ktoré sa používajú pre
spracovanie dát, ktoré majú mriežkovú topológiu ako napríklad obrazové dáta.
Využívajú sa najmä k rozpoznávaniu a klasifikácii obrázkov.
V prípade obrázkov sa jedná o dvojdimenzionálnu mriežku pixelov.
Ak sa jedná o čiernobiele obrázky, vstup je jednokanálový.
Pre farebné obrázky je vstup typicky trojkanálový.

Konvolučné siete sa skladajú z niekoľkých typov vrstiev, ktoré sú navzájom
poprepájané.
Základné a najčastejšie používané vrstvy sú: konvolučná (\textit{Convolutional}),
aktivačná (\textit{Activation}), zoskupovacia (\textit{Pooling}) a plne prepojená (\textit{Fully-connected}).
Tieto sú popísané podrobnejšie v ďalších odstavcoch.
V niektorých architektúrach sa môžu vyskytovať aj ďalšie vrstvy ako napríklad
vyrovnávacie (\textit{Flatten}), normalizačné (\textit{Local Response Normalization})
a vypúšťacie (\textit{Dropout}).

\paragraph{Konvolučná vrstva}

Základom konvolučnej vrstvy je operácia konvolúcie.
Konvolúcia pre dvojrozmerný vstupný obraz I a dvojrozmerný filter K
je daná vzťahom:
\begin{equation}\label{eq:conv}
    S(i,j) = (K*I)(i,j) = \sum_{m=a}^{b} \sum_{n=c}^{d} I(i-m,j-n) K(m,n)
\end{equation}
Parametre $a$, $b$, $c$, $d$ určujú veľkosť filtra.
Zo vzťahu~\ref{eq:conv} je zrejmé, že filter je otočený vzhľadom na vstup.
Toto otočenie je potrebné pre komutatívnu vlastnosť konvolúcie.
Keďže v konvolučnej neurónovej sieti nie je komutativita konvolúcie potrebná,
používa sa podobná funkcia nazývaná vzájomná korelácia
(\textit{cross-correlation}), ktorá nemá filter vzhľadom na vstup otočený:
\begin{equation}\label{eq:cross-cor}
    S(i,j) = (I*K)(i,j) = \sum_{m=a}^{b} \sum_{n=c}^{d} I(i+m,j+n) K(m,n)
\end{equation}
Pri strojovom učení sa učiaci algoritmus naučí príslušné váhy filtra
bezohľadu na to, či sa používa otočený filter alebo nie.
Taktiež ostatné funkcie, ktoré sa používajú v kombinácii s konvolučnou
vrstvou, sa správajú rovnako s otočeným aj neotočeným filtrom~\cite{Goodfellow-et-al-2016}.

\paragraph{Aktivačná vrstva}

Táto vrstva sa umiestňuje za konvolučnú vrstvu.
Výpočet v konvolučnej vrstve predstavuje bázovú funkciu neurónov
a výpočty v aktivačnej vrstve predstavujú výpočet aktivačnej funkcie
neurónov.
Ako aktivačná funkcia sa používa buď hyperbolický tangens alebo
jednoduchá usmerňovacia funkcia:

\begin{equation}\label{eq:relu}
    y = max(0,u)
\end{equation}

\paragraph{Zoskupovacia vrstva}

Používa sa na redukciu rozmerov obrazu.
Predchádzajúci výstup nahrádza súhrnnou hodnotou štatisticky
získanou z niekoľkých topologicky blízkych hodnôt.
Typicky sa používajú dve operácie -- priemerovanie alebo vyhľadávanie
maxima.
Parametrom tejto vrstvy je veľkosť oblasti, z ktorej sa počíta
súhrnná hodnota, a veľkosť posunutia.

\paragraph{Plne prepojená vrstva}

Je to štandardná dopredná sieť.
Používa sa lineárna bázová funkcia:

\begin{equation}\label{eq:lbf}
    u = \sum_{i = 1}^{n} w_i x_i
\end{equation}

Vo výstupných vrstvách sa ako aktivačná funkcia používa funkcia softmax:
\begin{equation}\label{eq:softmax}
    o_j = g(u_j) = \frac{e^{u_j}}{\sum_{k = 1}^{m} e^{u_k}}
\end{equation}
kde $o_j$ je výstup $j$-tého neurónu vrstvy, $m$ je počet neurónov vo vrstve,
$u$ je výstup bázovej funkcie neurónov.

Výhodnou vlastnosťou softmax funkcie je, že výstupné hodnoty sú v intervale
$<0, 1>$ a ich súčet je 1:

\begin{equation}\label{eq:softmax-property}
    \sum_{j = 1}^{m} o_j = 1
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Architektúra a popis implementácie}\label{sec:implementation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Demonštračná aplikácia bola napísaná v jazyku C++.
Zdrojové kódy sa skladajú z dvoch častí:
\begin{itemize}
    \item
        implementácia konvolučnej neurónovej siete a jednotlivých vrstiev
    \item
        aplikácia s grafickým užívateľským rozhraním
\end{itemize}

\subsection{Konvolučná neurónová sieť a jej vrstvy}

Naimplementované sú 4 základné vrstvy, ktoré sa používajú pri tvorbe konvolučných
neurónových sietí: konvolučná, aktivačná, zoskupovacia a plne prepojená.
Každá vrstva má určité parametre, ktoré sa zadávajú pri jej vytváraní.
Pre každú vrstvu sú určené rozmery vstupu: hĺbka, šírka a výška.
Okrem toho sú pre niektoré vrstvy zadané aj ďalšie parametre.

Pri vytváraní konvolučnej vrstvy sa určuje počet filtrov; hĺbka, šírka a výška
filtrov; krok (\textit{stride}), o ktorý sa posúva filter po vstupe; výplň (\textit{padding})
použitá okolo vstupnej matice; hodnota výplne.
Vrstvu je možné vytvoriť s pseudonáhodne nastavenými hodnotami váh vo filtri
alebo s určenými hodnotami.
Podľa zvolených parametrov sa vypočítajú rozmery výstupu vrstvy.

V aktivačnej vrstve sa nenastavujú žiadne ďalšie parametre okrem rozmerov vstupu.
Rozmery výstupu sú rovnaké ako rozmery vstupu.
Ako aktivačná funkcia je použitá jednoduchá usmerňovacia funkcia
(rovnica~\ref{eq:relu}).
Táto funkcia kopíruje na výstup kladné hodnoty vstupov a namiesto záporných
hodnôt je na výstupe 0.

Zoskupovacia vrstva má určenú výšku a šírku vstupnej oblasti, z ktorej
sú zoskupené hodnoty do výstupu jedného neurónu, a krok o koľko sa táto oblasť
posúva pre nasledujúci neurón.
Pre zoskupenie hodnôt zo vstupu sa dá zvoliť, či sa použije výpočet priemeru
zo vstupných hodnôt alebo maximum.
Rozmery výstupu sú vypočítané podľa rozmerov vstupu a zadaných parametrov.

Pri vytváraní plne prepojenej vrstvy sa zadávajú rozmery výstupu, čo znamená
počet neurónov tejto vrstvy.
Pre plne prepojenú vrstvu je podobne ako pre konvolučnú vrstvu možné zvoliť
hodnoty váh pri vytváraní vrstvy alebo nechať vygenerovať pseudonáhodné hodnoty.
Vrstva podporuje dve aktivačné funkcie: jednoduchú usmerňovaciu funkciu
(rovnica~\ref{eq:relu}) a softmax funkciu (rovnica~\ref{eq:softmax}).

Vymenované vrstvy je možné spájať za seba.
Spájané vrstvy musia mať navzájom kompatibilné vstupy a výstupy.
Nekompatibilné vrstvy nie je možné prepojiť.

Pri vytváraní konvolučnej neurónovej siete sú zvolené rozmery vstupnej vrstvy.
Do siete je následne možné pripájať a odpájať ďalšie vrstvy, tak aby bola
splnená podmienka kompatibility vstupov a výstupov.

Nad sieťou je následne možné vykonávať: naplnenie vstupnej vrstvy vstupnými hodnotami;
výpočet reakcie siete na vstup, pričom na konci tejto operácie je výstup poslednej
vrstvy výstupom siete; spätné propagovanie chyby sieťou spojené s aktualizáciou váh.

\subsection{Grafické užívateľské rozhranie pre demonštračnú aplikáciu}

S využitím naprogramovanej konvolučnej neurónovej siete a jej vrstiev bola vytvorená
aplikácia s grafickým užívateľským rozhraním, ktorá obsahuje ukážkovú architektúru
siete a umožňuje demonštráciu činnosti v tejto sieti.
Grafické rozhranie bolo naprogramované vo frameworku Qt\footnote{\url{https://www.qt.io/}}.
Použitá bola verzia Qt 5.
Aplikácia bola otestovaná na serveri \texttt{merlin} s verziou Qt 5.2.1.

V demonštračnej aplikácii je vytvorená konvolučná neurónová sieť, ktorá má za vstupnou
vrstvou napojenú 1 konvolučnú vrstvu, 1 aktivačnú vrstvu, 1 zoskupovaciu vrstvu
a na koniec výstupnú plne prepojenú vrstvu.

Na vstupe siete je matica s rozmermi \texttt{4x4}.
Konvolučná vrstva obsahuje 1 filter s rozmermi \texttt{3x3}, výplň má veľkosť 1 a používa krok 1.
Výstup konvolučnej vrstvy má teda opäť veľkosť \texttt{4x4}.
Rovnaké rozmery výstupu sú aj v aktivačnej vrstve.
Zoskupovacia vrstva zoskupuje oblasť s rozmermi \texttt{2x2} a teda výstup vrstvy má rozmery \texttt{2x2}.
Plne prepojená vrstva obsahuje 4 neuróny, pričom každý je spojený s každým neurónom
predchádzajúcej zoskupovacej vrstvy.
Architektúra siete je teda pevne daná.

Niektoré parametre vrstiev sú užívateľsky nastaviteľné.
Vo vstupnej vrstve sa dajú nastaviť hodnoty vstupov.
V konvolučnej vrstve je možné meniť hodnoty váh vo filtri.
V zoskupovacej vrstve sa dá zvoliť použitá funkcia, priemer alebo maximum.
V plne prepojenej vrstve sa dajú nastavovať hodnoty váh.

Grafické rozhranie aplikácie umožňuje demonštrovať postup výpočtov hodnôt výstupov
neurónov v jednotlivých vrstvách siete a postup pri propagácii chyby, a aktualizácii
váh v sieti.

Okno aplikácie má pevne nastavenú minimálnu veľkosť, aby mohli byť jednotlivé
prvky zmysluplne zobrazené.
Minimálna šírka okna je 1320 pixelov a výška 700 pixelov.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Použitie demonštračnej aplikácie}\label{sec:user_manual}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Aplikácia sa prekladá štandardne príkazom:
\begin{verbatim}
make
\end{verbatim}
Príkaz \texttt{make} zabezpečí, že pomocou nástroja \texttt{qmake} je vygenerovaný súbor
\texttt{Makefile-cnn\_demo}, nad ktorým je automaticky opäť spustený \texttt{make}.
Vznikne spustiteľný súbor \texttt{cnn\_demo}.
Okno s aplikáciou sa objaví po spustení:
\begin{verbatim}
./cnn_demo
\end{verbatim}

Okno obsahuje v hornej časti panel s nástrojmi, v ktorom je 5 tlačidiel:
\begin{itemize}
    \item \texttt{Init}

        Nastaví sieť do počiatočného stavu.
        Tlačidlo je aktívne vždy.

    \item \texttt{Run}

        Vypočíta výstupy celej siete a všetkých jej vrstiev pre daný vstup s aktuálne nastavenými
        váhami a parametrami.
        Tlačidlo je aktívne v počiatočnom stave.
        Počas priechodu spätnej propagácie chyby je tlačidlo neaktívne.

    \item \texttt{Step}

        Zobrazí výstup nasledujúceho neurónu.
        Pri stlačení tlačidla \texttt{Run} sú zobrazené výstupy aj všetkých ostávajúcich neurónov.
        Tlačidlo je aktívne rovnako ako tlačidlo \texttt{Run}.

    \item \texttt{Backprop Run}

        Propaguje chybu výstupu a aktualizuje všetky váhy v sieti.
        V počiatočnom stave je tlačidlo neaktívne.
        Tlačidlo je aktivované po zobrazení výstupov siete, čo nastane buď po stlačení tlačidla
        \texttt{Run} alebo po opakovanom použití tlačidla \texttt{Step} až po posledný neurón
        výstupnej vrstvy.
        Po prebehnutí aktualizácie váh je tlačidlo opäť neaktívne a pre jeho ďalšiu aktiváciu
        je nutné opäť použiť tlačidlá \texttt{Run} alebo \texttt{Step}.

    \item \texttt{Backprop Step}

        Propaguje chybu výstupu a aktualizuje jednu nasledujúcu váhu v sieti.
        Tlačidlo je aktivované a deaktivované rovnako ako tlačidlo \texttt{Backprop Run}.
        Ak je použité tlačidlo \texttt{Backprop Run}, tak sú aktualizované aj všetky ostávajúce
        váhy v sieti.
\end{itemize}

V sieti je možné nastavovať ručne hodnoty vstupov, filtru v konvolučnej sieti a váh v plne prepojenej vrstve.
Po kliknutí na zvolené políčko s hodnotou sa zobrazí prvok umožňujúci zadať hodnotu políčka buď ako číslo
pomocou klávesnice alebo použitím šípok hore a dole.
Zmenu hodnoty je potrebné potvrdiť buď klávesou \texttt{Enter} alebo kliknutím na iné políčko v sieti,
napríklad aj výstupy vrstiev (tie však nie je možné nastavovať).

V zoskupovacej vrstve je možné vybrať použitie zoskupovania s využitím priemeru alebo maximálnej hodnoty.

Napravo od výstupov siete v plne prepojenej vrstve sú zobrazené políčka pre nastavenie očakávaných
výstupných hodnôt, ktoré sa používajú pri spätnej propagácii chyby.
Dole pod plne prepojenou vrstvou sa dá nastaviť hodnota koeficientu učenia.

Počas výpočtu výstupov v sieti je červenou farbou zvýraznený aktuálne vypočítaný výstup,
zelenou vstupné hodnoty a modrou váhy použité pre výpočet tohto výstupu.
Pri spätnej propagácii chyby je modrou farbou zvýraznená práve aktualizovaná váha.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Záver}\label{sec:conclusion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

V tomto projekte boli naprogramované 4 základné vrstvy (konvolučná, aktivačná,
zoskupovacia, plne prepojená), ktoré je možné spájať a vytvoriť z nich konvolučnú
neurónovú sieť.
S využitím týchto vrstiev bola vytvorená aplikácia s grafickým užívateľským rozhraním,
ktorá umožňuje demonštrovať výpočet výstupov siete a spätnú propagáciu chyby
s aktualizáciami váh.
V aplikácii je možné nastavovať hodnoty vstupu siete, váh v jednotlivých vrstvách
a sledovať výstupy vrstiev.

V texte je popísaná architektúra naimplementovanej konvolučnej neurónovej
siete a demonštračnej aplikácie, a taktiež návod k použitiu vytvorenej aplikácie.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
