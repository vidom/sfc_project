#! /bin/bash

NAME="doc"
if [ -n "$1" ]
then
    NAME="$1"
fi

mv *.bib ${NAME}.bib
mv *.tex ${NAME}.tex
sed -i "s/^NAME=.*$/NAME=${NAME}/" Makefile
sed -i -r "s/^(%?)\\\\bibliography\{.*\}$/\1\\\\bibliography{${NAME}}/" ${NAME}.tex
