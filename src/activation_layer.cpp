/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <iostream>

/* Own headers. */
#include "debug.h"
#include "layer.h"
#include "activation_layer.h"

using namespace std;

Activation_layer::Activation_layer(Dimensions di)
{
    this->prev = nullptr;
    this->next = nullptr;
    this->dim_in = di;
    this->dim_out = di;
    this->output = vector< vector< vector<outval_t> > >(
        di.depth, vector< vector<outval_t> >(di.height, vector<outval_t>(di.width)));

    CNN_DEBUG_MSG("Activation_layer is created!");
}

Activation_layer::~Activation_layer(void)
{
    CNN_DEBUG_MSG("Activation_layer is destroyed!");
}

outval_t
Activation_layer::get_neuron_output(unsigned int d,
        unsigned int h, unsigned int w)
{
    return (this->prev->output[d][h][w] < 0) ? 0 : this->prev->output[d][h][w];
}

void
Activation_layer::backprop(double mi, vector<vector<vector<double> > > deltas)
{
    vector<vector<vector<double>>> new_deltas = vector<vector<vector<double>>>(
            this->prev->dim_out.depth, vector<vector<double>>(this->prev->dim_out.height,
            vector<double>(this->prev->dim_out.width, 0)));
    /* Count deltas for previous layer. */
    for (unsigned int d = 0; d < this->prev->dim_out.depth; d++) {
        for (unsigned int h = 0; h < this->prev->dim_out.height; h++) {
            for (unsigned int w = 0; w < this->prev->dim_out.width; w++) {
                if (this->prev->output[d][h][w] > 0) {
                    new_deltas[d][h][w] = deltas[d][h][w];
                }
            }
        }
    }
    this->prev->backprop(mi, new_deltas);
}

void
Activation_layer::print(ostream &stream)
{
    stream << "--Activation Layer--" << endl;
    this->print_dim(stream);
}
