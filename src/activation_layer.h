/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef __ACTIVATION_LAYER_H__
#define __ACTIVATION_LAYER_H__

/* System and standard library headers. */
#include <iostream>
#include <vector>

/* Own headers. */
#include "layer.h"

using namespace std;

/**
 * @brief Activation Layer
 */
class Activation_layer: public Layer {
public:
    Activation_layer(Dimensions di);
    ~Activation_layer(void);

    outval_t get_neuron_output(unsigned int d, unsigned int h, unsigned int w);
    void backprop(double mi, vector<vector<vector<double>>> deltas);

    void print(ostream &stream);
};

#endif /* __ACTIVATION_LAYER_H__ */
