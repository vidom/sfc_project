/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <algorithm>

/* Qt libraries headers. */
#include <QGraphicsView>
#include <QGraphicsTextItem>
#include <QWidget>
#include <QMessageBox>
#include <QDebug>
#include <QtAlgorithms>

/* Own headers. */
#include "cnn_demo.h"
#include "ui_cnn_demo.h"

using namespace std;

template <typename T>
string to_string_with_precision(const T a_value, const int n = 6)
{
    ostringstream out;
    out << fixed << setprecision(n) << a_value;
    return out.str();
}

QColor my_blue = QColor(127, 127, 255);
QColor my_green = QColor(127, 255, 127);
QColor my_red = QColor(255, 127, 127);

GraphicsScene::GraphicsScene(QObject *parent, Cnn_demo *cd) :
    QGraphicsScene(parent),
    cnn_demo(cd)
{
}

void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
    //qDebug() << Q_FUNC_INFO << mouseEvent->scenePos();
    unsigned int size = this->cnn_demo->input_size;
    unsigned int padding = this->cnn_demo->conv_padding;
    unsigned int x = (mouseEvent->scenePos().x() >= 0) ? mouseEvent->scenePos().x() : 0;
    unsigned int y = (mouseEvent->scenePos().y() >= 0) ? mouseEvent->scenePos().y() : 0;
    unsigned int c = (x / 40);
    unsigned int r = (y / 40);

    if (this->cnn_demo->backprop_started == false) {
        if (this == this->cnn_demo->input_scene) {
            if (!(padding > 0 && (r < padding || r >= size + padding ||
                    c < padding || c >= size + padding))) {
                c = c - padding;
                r = r - padding;
                this->cnn_demo->input_spinboxes[r][c]->show();
                this->cnn_demo->input_spinboxes[r][c]->setFocus();
            }
        } else if (this == this->cnn_demo->conv_filter_scene) {
            this->cnn_demo->conv_filter_spinboxes[r][c]->show();
            this->cnn_demo->conv_filter_spinboxes[r][c]->setFocus();
        } else if (this == this->cnn_demo->conv_bias_scene) {
            this->cnn_demo->conv_bias_spinbox->show();
            this->cnn_demo->conv_bias_spinbox->setFocus();
        } else if (find(this->cnn_demo->fc_weight_scene.begin(), this->cnn_demo->fc_weight_scene.end(),
                this) != this->cnn_demo->fc_weight_scene.end()) {
            unsigned int d = find(this->cnn_demo->fc_weight_scene.begin(),
                    this->cnn_demo->fc_weight_scene.end(), this) -
                    this->cnn_demo->fc_weight_scene.begin();
            this->cnn_demo->fc_weight_spinboxes[d][r][c]->show();
            this->cnn_demo->fc_weight_spinboxes[d][r][c]->setFocus();
            this->cnn_demo->current_sb_depth = d;
        } else if (find(this->cnn_demo->fc_bias_scene.begin(), this->cnn_demo->fc_bias_scene.end(),
                this) != this->cnn_demo->fc_bias_scene.end()) {
            unsigned int d = find(this->cnn_demo->fc_bias_scene.begin(),
                    this->cnn_demo->fc_bias_scene.end(), this) -
                    this->cnn_demo->fc_bias_scene.begin();
            this->cnn_demo->fc_bias_spinbox[d]->show();
            this->cnn_demo->fc_bias_spinbox[d]->setFocus();
            this->cnn_demo->current_sb_depth = d;
        }
        this->cnn_demo->current_sb_col = c;
        this->cnn_demo->current_sb_row = r;
        //qDebug() << Q_FUNC_INFO << "r: " << r << " c: " << c;
    }

    QGraphicsScene::mousePressEvent(mouseEvent);
}

Cnn_demo::Cnn_demo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Cnn_demo),
    input_size{4},
    conv_padding{1},
    conv_stride{1},
    conv_filter_h{3},
    conv_filter_w{3},
    pool_stride{2},
    pool_height{2},
    pool_width{2},
    fc_size{4},
    conv_net{nullptr},
    conv_layer{nullptr},
    act_layer{nullptr},
    pool_layer{nullptr},
    fc_layer{nullptr},
    input_view{nullptr},
    input_scene{nullptr},
    input_texts{},
    input_spinboxes{},
    current_sb_row{0},
    current_sb_col{0},
    current_sb_depth{0},
    conv_out_view{nullptr},
    conv_filter_view{nullptr},
    conv_bias_view{nullptr},
    conv_out_scene{nullptr},
    conv_filter_scene{nullptr},
    conv_bias_scene{nullptr},
    conv_out_texts{},
    conv_filter_texts{},
    conv_bias_text{nullptr},
    conv_filter_spinboxes{},
    conv_bias_spinbox{nullptr},
    act_view{nullptr},
    act_scene{nullptr},
    act_texts{},
    pool_view{nullptr},
    pool_scene{nullptr},
    pool_texts{},
    fc_weight_view{fc_size, nullptr},
    fc_bias_view{fc_size, nullptr},
    fc_out_view{fc_size, nullptr},
    fc_weight_scene{fc_size, nullptr},
    fc_bias_scene{fc_size, nullptr},
    fc_out_scene{fc_size, nullptr},
    fc_weight_texts{fc_size},
    fc_bias_text{fc_size, nullptr},
    fc_out_text{fc_size, nullptr},
    fc_weight_spinboxes{fc_size},
    fc_bias_spinbox{fc_size, nullptr},
    fc_lines{fc_size + 1, nullptr},
    expected_values_spinboxes{fc_size, nullptr},
    current_layer{nullptr},
    current_output{0, 0, 0},
    backprop_started{false},
    backprop_current_item{FC_WEIGHT},
    backprop_current_dim{0, 0, 0}
{
    this->ui->setupUi(this);
    this->setWindowTitle("CNN demo");
    this->ui->label_in_size->setText(this->ui->label_in_size->text() +
            QString::fromStdString(to_string(this->input_size)) + "x" +
            QString::fromStdString(to_string(this->input_size)));
    this->ui->label_conv_fh->setText(this->ui->label_conv_fh->text() +
            QString::fromStdString(to_string(this->conv_filter_h)));
    this->ui->label_conv_fw->setText(this->ui->label_conv_fw->text() +
            QString::fromStdString(to_string(this->conv_filter_w)));
    this->ui->label_conv_padding->setText(this->ui->label_conv_padding->text() +
            QString::fromStdString(to_string(this->conv_padding)));
    this->ui->label_conv_stride->setText(this->ui->label_conv_stride->text() +
            QString::fromStdString(to_string(this->conv_stride)));
    this->ui->label_pool_height->setText(this->ui->label_pool_height->text() +
            QString::fromStdString(to_string(this->pool_height)));
    this->ui->label_pool_width->setText(this->ui->label_pool_width->text() +
            QString::fromStdString(to_string(this->pool_width)));
    this->ui->label_pool_stride->setText(this->ui->label_pool_stride->text() +
            QString::fromStdString(to_string(this->pool_stride)));

    this->input_view = new QGraphicsView(this);
    this->input_scene = new GraphicsScene(this->input_view, this);
    this->input_view->setScene(this->input_scene);
    this->input_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->input_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->init_input();

    connect(this->ui->doubleSpinBox_learning_rate, SIGNAL(editingFinished()),
            this, SLOT(on_learning_rate_spinBox_editingFinished()));

    this->conv_filter_view = new QGraphicsView(this);
    this->conv_bias_view = new QGraphicsView(this);
    this->conv_out_view = new QGraphicsView(this);
    this->conv_filter_scene = new GraphicsScene(this->conv_filter_view, this);
    this->conv_bias_scene = new GraphicsScene(this->conv_bias_view, this);
    this->conv_out_scene = new GraphicsScene(this->conv_out_view, this);
    this->conv_filter_view->setScene(this->conv_filter_scene);
    this->conv_bias_view->setScene(this->conv_bias_scene);
    this->conv_out_view->setScene(this->conv_out_scene);
    this->conv_filter_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->conv_filter_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->conv_bias_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->conv_bias_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->conv_out_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->conv_out_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->init_conv();

    this->act_view = new QGraphicsView(this);
    this->act_scene = new GraphicsScene(this->act_view, this);
    this->act_view->setScene(this->act_scene);
    this->act_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->act_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->init_act();

    this->pool_view = new QGraphicsView(this);
    this->pool_scene = new GraphicsScene(this->pool_view, this);
    this->pool_view->setScene(this->pool_scene);
    this->pool_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->pool_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->init_pool();

    unsigned int d = 0;
    for (d = 0; d < this->fc_size; d++) {
        this->fc_weight_view[d] = new QGraphicsView(this);
        this->fc_bias_view[d] = new QGraphicsView(this);
        this->fc_out_view[d] = new QGraphicsView(this);
        this->fc_weight_scene[d] = new GraphicsScene(this->fc_weight_view[d], this);
        this->fc_bias_scene[d] = new GraphicsScene(this->fc_bias_view[d], this);
        this->fc_out_scene[d] = new GraphicsScene(this->fc_out_view[d], this);
        this->fc_weight_view[d]->setScene(this->fc_weight_scene[d]);
        this->fc_weight_view[d]->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->fc_weight_view[d]->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->fc_bias_view[d]->setScene(this->fc_bias_scene[d]);
        this->fc_bias_view[d]->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->fc_bias_view[d]->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->fc_out_view[d]->setScene(this->fc_out_scene[d]);
        this->fc_out_view[d]->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->fc_out_view[d]->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->fc_lines[d] = new QFrame(this);
        this->fc_lines[d]->setGeometry(930, 120 + d * 120, 380, 16);
        this->fc_lines[d]->setFrameShape(QFrame::HLine);
        this->fc_lines[d]->setFrameShadow(QFrame::Sunken);
        this->expected_values_spinboxes[d] = new QDoubleSpinBox(this);
        this->expected_values_spinboxes[d]->setGeometry(1230, 140 + d * 120, 60 + 4, 40 + 4);
        this->expected_values_spinboxes[d]->setValue((d == 0) ? 1 : 0);
        this->expected_values_spinboxes[d]->setMinimum(0);
        this->expected_values_spinboxes[d]->setMaximum(1);
        this->expected_values_spinboxes[d]->setDecimals(1);
        this->expected_values_spinboxes[d]->setSingleStep(1);
    }
    this->fc_lines[d] = new QFrame(this);
    this->fc_lines[d]->setGeometry(930, 120 + d * 120, 380, 16);
    this->fc_lines[d]->setFrameShape(QFrame::HLine);
    this->fc_lines[d]->setFrameShadow(QFrame::Sunken);
    this->init_fc();

    this->current_layer = &(this->conv_net->input_layer);

    this->disable_backprop();
}

Cnn_demo::~Cnn_demo(void)
{
    delete ui;
    if (this->conv_net != nullptr) {
        delete this->conv_net;
    }
}

void
Cnn_demo::init_backprop(void)
{
    this->backprop_current_item = FC_WEIGHT;
    this->backprop_current_dim = Dimensions{0, 0, 0};
}

void
Cnn_demo::disable_backprop(void)
{
    this->ui->actionBackprop_Run->setDisabled(true);
    this->ui->actionBackprop_Step->setDisabled(true);
    this->backprop_started = false;
}

void
Cnn_demo::enable_backprop(void)
{
    this->ui->actionBackprop_Run->setEnabled(true);
    this->ui->actionBackprop_Step->setEnabled(true);
    this->init_backprop();
}

void
Cnn_demo::disable_forward(void)
{
    this->ui->actionRun->setDisabled(true);
    this->ui->actionStep->setDisabled(true);
}

void
Cnn_demo::enable_forward(void)
{
    this->ui->actionRun->setEnabled(true);
    this->ui->actionStep->setEnabled(true);
}

void
Cnn_demo::init_input(void)
{
    unsigned int size = this->input_size;

    if (this->conv_net != nullptr) {
        delete this->conv_net;
        this->conv_layer = nullptr;
        this->act_layer = nullptr;
        this->pool_layer = nullptr;
        this->fc_layer = nullptr;
    }
    this->conv_net = new Conv_net(Dimensions{1U, size, size}, this->ui->doubleSpinBox_learning_rate->value());
    this->conv_net->feed_input(vector<vector<vector<outval_t>>>(1,
            vector<vector<outval_t>>(size, vector<outval_t>(size, 1))));

    this->input_texts = vector<vector<QGraphicsTextItem *>>(size,
            vector<QGraphicsTextItem *>(size, nullptr));

    for (auto &a: this->input_spinboxes) {
        for (auto &b: a) {
            if (b != nullptr) {
                delete b;
            }
        }
    }
    this->input_spinboxes = vector<vector<QDoubleSpinBox *>>(size,
            vector<QDoubleSpinBox *>(size, nullptr));

    this->input_view->setGeometry(50, 410, 40 * size + 4, 40 * size + 4);
    this->input_scene->clear();
    for (unsigned int r = 0; r < size; r++) {
        for (unsigned int c = 0; c < size; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->input_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
            this->input_texts[r][c] = this->input_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->conv_net->input_layer.input[0][r][c], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->input_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);

            this->input_spinboxes[r][c] = new QDoubleSpinBox(this->input_view);
            this->input_spinboxes[r][c]->setGeometry(40 * c + 2, 40 * r + 2, 40, 40);
            this->input_spinboxes[r][c]->setValue(this->conv_net->input_layer.input[0][r][c]);
            this->input_spinboxes[r][c]->setMinimum(-2000000000);
            this->input_spinboxes[r][c]->setMaximum(2000000000);
            this->input_spinboxes[r][c]->setDecimals(1);
            this->input_spinboxes[r][c]->hide();

            connect(this->input_spinboxes[r][c], SIGNAL(editingFinished()),
                    this, SLOT(on_input_spinBox_editingFinished()));
        }
    }
}

void
Cnn_demo::init_conv(void)
{
    unsigned int padding = this->conv_padding;
    unsigned int stride = this->conv_stride;
    unsigned int filter_h = this->conv_filter_h;
    unsigned int filter_w = this->conv_filter_w;
    unsigned int in_h = this->conv_net->last_layer->dim_out.height;
    unsigned int in_w = this->conv_net->last_layer->dim_out.width;

    try {
        this->conv_layer = new Conv_layer(Dimensions{1U, in_h, in_w}, 1, filter_h,
                filter_w, stride, padding, 0);
    } catch (Layer_exception &e) {
        cerr << "Error in creating Convolutional Layer" << endl;
    }

    unsigned int out_h = this->conv_layer->dim_out.height;
    unsigned int out_w = this->conv_layer->dim_out.width;

    int ret = this->conv_net->append_layer(this->conv_layer);
    if (ret != 0) {
        cerr << "Error in appending Convolutional Layer!" << endl;
    }

    for (auto &a: this->conv_filter_spinboxes) {
        for (auto &b: a) {
            if (b != nullptr) {
                delete b;
            }
        }
    }
    if (this->conv_bias_spinbox != nullptr) {
        delete this->conv_bias_spinbox;
    }

    this->conv_filter_spinboxes = vector<vector<QDoubleSpinBox *>>(filter_h,
            vector<QDoubleSpinBox *>(filter_w, nullptr));
    this->conv_bias_spinbox = nullptr;

    this->conv_filter_texts = vector<vector<QGraphicsTextItem *>>(filter_h,
            vector<QGraphicsTextItem *>(filter_w, nullptr));
    this->conv_out_texts = vector<vector<QGraphicsTextItem *>>(out_h,
            vector<QGraphicsTextItem *>(out_w, nullptr));
    this->conv_bias_text = nullptr;

    this->conv_filter_view->setGeometry(280, 220, 40 * filter_w + 4, 40 * filter_h + 4);
    this->conv_filter_scene->clear();
    for (unsigned int r = 0; r < filter_h; r++) {
        for (unsigned int c = 0; c < filter_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->conv_filter_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
            this->conv_filter_texts[r][c] = this->conv_filter_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->conv_layer->filters[0][0][r][c], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->conv_filter_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);

            this->conv_filter_spinboxes[r][c] = new QDoubleSpinBox(this->conv_filter_view);
            this->conv_filter_spinboxes[r][c]->setGeometry(40 * c + 2, 40 * r + 2, 40, 40);
            this->conv_filter_spinboxes[r][c]->setValue(this->conv_layer->filters[0][0][r][c]);
            this->conv_filter_spinboxes[r][c]->setMinimum(-2000000000);
            this->conv_filter_spinboxes[r][c]->setMaximum(2000000000);
            this->conv_filter_spinboxes[r][c]->setDecimals(1);
            this->conv_filter_spinboxes[r][c]->hide();

            connect(this->conv_filter_spinboxes[r][c], SIGNAL(editingFinished()),
                    this, SLOT(on_conv_filter_spinBox_editingFinished()));
        }
    }

    this->conv_bias_view->setGeometry(430, 220, 40 + 4, 40 + 4);
    this->conv_bias_scene->clear();
    {
        QBrush brush(Qt::white);
        QPen pen(Qt::black);
        pen.setWidth(2);
        this->conv_bias_scene->addRect(0, 0, 40, 40, pen, brush);
        this->conv_bias_text = this->conv_bias_scene->addText(QString::fromStdString(
                to_string_with_precision(this->conv_layer->biases[0], 1)),
                QFont("Times", 11, QFont::Bold));
        this->conv_bias_text->setPos(0 + 5, 0 + 5);

        this->conv_bias_spinbox = new QDoubleSpinBox(this->conv_bias_view);
        this->conv_bias_spinbox->setGeometry(0 + 2, 0 + 2, 40, 40);
        this->conv_bias_spinbox->setValue(this->conv_layer->biases[0]);
        this->conv_bias_spinbox->setMinimum(-2000000000);
        this->conv_bias_spinbox->setMaximum(2000000000);
        this->conv_bias_spinbox->setDecimals(1);
        this->conv_bias_spinbox->hide();

        connect(this->conv_bias_spinbox, SIGNAL(editingFinished()),
                this, SLOT(on_conv_bias_spinBox_editingFinished()));
    }

    this->conv_out_view->setGeometry(280, 410, 40 * out_w + 4, 40 * out_h + 4);
    this->conv_out_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->conv_out_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
        }
    }

    if (padding > 0) {
        unsigned int size = this->input_size;
        this->input_view->setGeometry(50 - 40 * padding, 410 - 40 * padding,
                40 * (size + 2 * padding) + 4, 40 * (size + 2 * padding) + 4);
        this->input_scene->clear();
        for (unsigned int r = 0; r < size + 2 * padding; r++) {
            for (unsigned int c = 0; c < size + 2 * padding; c++) {
                if (r < padding || c < padding || r >= padding + size || c >= padding + size) {
                    QBrush brush(Qt::white);
                    QPen pen(Qt::gray);
                    pen.setWidth(1);
                    pen.setStyle(Qt::DashLine);
                    this->input_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                    QGraphicsTextItem *text = this->input_scene->addText(QString::fromStdString(
                            to_string_with_precision(this->conv_layer->padding_val, 1)),
                            QFont("Times", 11, QFont::Bold));
                    text->setPos(40 * c + 5, 40 * r + 5);
                    text->setDefaultTextColor(Qt::gray);
                }
            }
        }
        for (unsigned int r = 0; r < size; r++) {
            for (unsigned int c = 0; c < size; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                pen.setWidth(2);
                this->input_scene->addRect(40 * (c + padding), 40 * (r + padding), 40, 40, pen, brush);
                this->input_texts[r][c] = this->input_scene->addText(QString::fromStdString(
                        to_string_with_precision(this->conv_net->input_layer.input[0][r][c], 1)),
                        QFont("Times", 11, QFont::Bold));
                this->input_texts[r][c]->setPos(40 * (c + padding) + 5, 40 * (r + padding) + 5);

                this->input_spinboxes[r][c]->setGeometry(40 * (c + padding) + 2,
                        40 * (r + padding) + 2, 40, 40);
                this->input_spinboxes[r][c]->setValue(this->conv_net->input_layer.input[0][r][c]);
                this->input_spinboxes[r][c]->setMinimum(-2000000000);
                this->input_spinboxes[r][c]->setMaximum(2000000000);
                this->input_spinboxes[r][c]->setDecimals(1);
                this->input_spinboxes[r][c]->hide();

                connect(this->input_spinboxes[r][c], SIGNAL(editingFinished()),
                        this, SLOT(on_input_spinBox_editingFinished()));
            }
        }
    }
}

void
Cnn_demo::reset_conv_out(void)
{
    unsigned int out_h = this->conv_layer->dim_out.height;
    unsigned int out_w = this->conv_layer->dim_out.width;
    this->conv_out_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->conv_out_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
        }
    }
}

void
Cnn_demo::init_act(void)
{
    unsigned int out_h = this->conv_net->last_layer->dim_out.height;
    unsigned int out_w = this->conv_net->last_layer->dim_out.width;

    try {
        this->act_layer = new Activation_layer(Dimensions{1U, out_h, out_w});
    } catch (Layer_exception &e) {
        cerr << "Error in creating Activation Layer" << endl;
    }

    int ret = this->conv_net->append_layer(this->act_layer);
    if (ret != 0) {
        cerr << "Error in appending Activation Layer!" << endl;
    }

    this->act_texts = vector<vector<QGraphicsTextItem *>>(out_h,
            vector<QGraphicsTextItem *>(out_w, nullptr));

    this->act_view->setGeometry(500, 410, 40 * out_h + 4, 40 * out_w + 4);
    this->act_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->act_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
        }
    }
}

void
Cnn_demo::reset_act_out(void)
{
    unsigned int out_h = this->act_layer->dim_out.height;
    unsigned int out_w = this->act_layer->dim_out.width;
    this->act_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->act_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
        }
    }
}

void
Cnn_demo::init_pool(void)
{
    unsigned int in_h = this->conv_net->last_layer->dim_out.height;
    unsigned int in_w = this->conv_net->last_layer->dim_out.width;
    Pooling_layer::operation_type ot = (this->ui->comboBox_pool_op->currentIndex() == 0) ?
                Pooling_layer::MAX : Pooling_layer::AVERAGE;

    try {
        this->pool_layer = new Pooling_layer(Dimensions{1U, in_h, in_w}, pool_height,
                pool_width, pool_stride, ot);
    } catch (Layer_exception &e) {
        cerr << "Error in creating Pooling Layer" << endl;
    }

    unsigned int out_h = this->pool_layer->dim_out.height;
    unsigned int out_w = this->pool_layer->dim_out.width;

    int ret = this->conv_net->append_layer(this->pool_layer);
    if (ret != 0) {
        cerr << "Error in appending Pooling Layer!" << endl;
    }

    this->pool_texts = vector<vector<QGraphicsTextItem *>>(out_h,
            vector<QGraphicsTextItem *>(out_w, nullptr));

    this->pool_view->setGeometry(720, 410, 40 * out_h + 4, 40 * out_w + 4);
    this->pool_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->pool_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
        }
    }

    connect(this->ui->comboBox_pool_op, SIGNAL(currentIndexChanged(int)),
            this, SLOT(on_pool_op_comboBox_currentIndexChanged(int)));
}

void
Cnn_demo::reset_pool_out(void)
{
    unsigned int out_h = this->pool_layer->dim_out.height;
    unsigned int out_w = this->pool_layer->dim_out.width;
    this->pool_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->pool_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
        }
    }
}

void
Cnn_demo::init_fc(void)
{
    unsigned int in_h = this->conv_net->last_layer->dim_out.height;
    unsigned int in_w = this->conv_net->last_layer->dim_out.width;

    try {
        this->fc_layer = new Fc_layer(Dimensions{1U, in_h, in_w}, Dimensions{this->fc_size, 1U, 1U},
                Fc_layer::SOFTMAX);
    } catch (Layer_exception &e) {
        cerr << "Error in creating Fully-connected Layer" << endl;
    }

    int ret = this->conv_net->append_layer(this->fc_layer);
    if (ret != 0) {
        cerr << "Error in appending Fully-connected Layer!" << endl;
    }

    for (unsigned int d = 0; d < this->fc_size; d++) {
        for (auto &a: this->fc_weight_spinboxes[d]) {
            for (auto &b: a) {
                if (b != nullptr) {
                    delete b;
                }
            }
        }
        if (this->fc_bias_spinbox[d] != nullptr) {
            delete this->fc_bias_spinbox[d];
        }

        this->fc_weight_texts[d] = vector<vector<QGraphicsTextItem *>>(in_h,
                vector<QGraphicsTextItem *>(in_w, nullptr));

        this->fc_weight_spinboxes[d] = vector<vector<QDoubleSpinBox *>>(in_h,
                vector<QDoubleSpinBox *>(in_w, nullptr));

        this->fc_bias_text[d] = nullptr;

        this->fc_bias_spinbox[d] = nullptr;

        this->fc_out_text[d] = nullptr;

        this->fc_weight_view[d]->setGeometry(940, 140 + d * 120, 40 * in_w + 4, 40 * in_h + 4);
        this->fc_weight_scene[d]->clear();
        for (unsigned int r = 0; r < in_h; r++) {
            for (unsigned int c = 0; c < in_w; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                pen.setWidth(2);
                this->fc_weight_scene[d]->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                this->fc_weight_texts[d][r][c] = this->fc_weight_scene[d]->addText(QString::fromStdString(
                        to_string_with_precision(this->fc_layer->weights[d][r * in_w + c], 1)),
                        QFont("Times", 11, QFont::Bold));
                this->fc_weight_texts[d][r][c]->setPos(40 * c + 5, 40 * r + 5);

                this->fc_weight_spinboxes[d][r][c] = new QDoubleSpinBox(this->fc_weight_view[d]);
                this->fc_weight_spinboxes[d][r][c]->setGeometry(40 * c + 2, 40 * r + 2, 40, 40);
                this->fc_weight_spinboxes[d][r][c]->setValue(this->fc_layer->weights[d][r * in_w + c]);
                this->fc_weight_spinboxes[d][r][c]->setMinimum(-2000000000);
                this->fc_weight_spinboxes[d][r][c]->setMaximum(2000000000);
                this->fc_weight_spinboxes[d][r][c]->setDecimals(1);
                this->fc_weight_spinboxes[d][r][c]->hide();

                connect(this->fc_weight_spinboxes[d][r][c], SIGNAL(editingFinished()),
                        this, SLOT(on_fc_weight_spinBox_editingFinished()));
            }
        }

        this->fc_bias_view[d]->setGeometry(1060, 140 + d * 120, 40 * 1 + 4, 40 * 1 + 4);
        this->fc_bias_scene[d]->clear();
        {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->fc_bias_scene[d]->addRect(0, 0, 40, 40, pen, brush);
            this->fc_bias_text[d] = this->fc_bias_scene[d]->addText(QString::fromStdString(
                    to_string_with_precision(this->fc_layer->biases[d], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->fc_bias_text[d]->setPos(0 + 5, 0 + 5);

            this->fc_bias_spinbox[d] = new QDoubleSpinBox(this->fc_bias_view[d]);
            this->fc_bias_spinbox[d]->setGeometry(0 + 2, 0 + 2, 40, 40);
            this->fc_bias_spinbox[d]->setValue(this->fc_layer->biases[d]);
            this->fc_bias_spinbox[d]->setMinimum(-2000000000);
            this->fc_bias_spinbox[d]->setMaximum(2000000000);
            this->fc_bias_spinbox[d]->setDecimals(1);
            this->fc_bias_spinbox[d]->hide();

            connect(this->fc_bias_spinbox[d], SIGNAL(editingFinished()),
                    this, SLOT(on_fc_bias_spinBox_editingFinished()));
        }

        this->fc_out_view[d]->setGeometry(1140, 140 + d * 120, 60 + 4, 40 + 4);
        this->fc_out_scene[d]->clear();
        {
            QBrush brush(Qt::lightGray);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->fc_out_scene[d]->addRect(0, 0, 60, 40, pen, brush);
        }

        this->expected_values_spinboxes[d]->setValue((d == 0) ? 1 : 0);
    }
}

void
Cnn_demo::reset_fc_out(void)
{
    for (unsigned int d = 0; d < this->fc_size; d++) {
        this->fc_out_scene[d]->clear();
        {
            QBrush brush(Qt::lightGray);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->fc_out_scene[d]->addRect(0, 0, 60, 40, pen, brush);
        }
    }
}

void
Cnn_demo::actualize_input(QColor input_color)
{
    unsigned int padding = this->conv_padding;
    unsigned int size = this->input_size;

    this->input_view->setGeometry(50 - 40 * padding, 410 - 40 * padding,
            40 * (size + 2 * padding) + 4, 40 * (size + 2 * padding) + 4);
    this->input_scene->clear();
    for (unsigned int r = 0; r < size + 2 * padding; r++) {
        for (unsigned int c = 0; c < size + 2 * padding; c++) {
            if (r < padding || c < padding || r >= padding + size || c >= padding + size) {
                QBrush brush(Qt::white);
                QPen pen(Qt::gray);
                pen.setWidth(1);
                pen.setStyle(Qt::DashLine);
                this->input_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                QGraphicsTextItem *text = this->input_scene->addText(QString::fromStdString(
                        to_string_with_precision(this->conv_layer->padding_val, 1)),
                        QFont("Times", 11, QFont::Bold));
                text->setPos(40 * c + 5, 40 * r + 5);
                text->setDefaultTextColor(Qt::gray);
            }
        }
    }
    for (unsigned int r = 0; r < size; r++) {
        for (unsigned int c = 0; c < size; c++) {
            QBrush brush(input_color);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->input_scene->addRect(40 * (c + padding), 40 * (r + padding), 40, 40, pen, brush);
            this->input_texts[r][c] = this->input_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->conv_net->input_layer.input[0][r][c], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->input_texts[r][c]->setPos(40 * (c + padding) + 5, 40 * (r + padding) + 5);

            this->input_spinboxes[r][c]->setGeometry(40 * (c + padding) + 2,
                    40 * (r + padding) + 2, 40, 40);
            this->input_spinboxes[r][c]->setValue(this->conv_net->input_layer.input[0][r][c]);
            this->input_spinboxes[r][c]->hide();
        }
    }
}

void
Cnn_demo::actualize_conv_out(void)
{
    unsigned int out_h = this->conv_layer->dim_out.height;
    unsigned int out_w = this->conv_layer->dim_out.width;
    unsigned int filter_h = this->conv_filter_h;
    unsigned int filter_w = this->conv_filter_w;
    this->conv_filter_scene->clear();
    for (unsigned int r = 0; r < filter_h; r++) {
        for (unsigned int c = 0; c < filter_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->conv_filter_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
            this->conv_filter_texts[r][c] = this->conv_filter_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->conv_layer->filters[0][0][r][c], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->conv_filter_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
            this->conv_filter_spinboxes[r][c]->setValue(this->conv_layer->filters[0][0][r][c]);
            this->conv_filter_spinboxes[r][c]->hide();
        }
    }
    this->conv_bias_scene->clear();
    {
        QBrush brush(Qt::white);
        QPen pen(Qt::black);
        pen.setWidth(2);
        this->conv_bias_scene->addRect(0, 0, 40, 40, pen, brush);
        this->conv_bias_text = this->conv_bias_scene->addText(QString::fromStdString(
                to_string_with_precision(this->conv_layer->biases[0], 1)),
                QFont("Times", 11, QFont::Bold));
        this->conv_bias_text->setPos(0 + 5, 0 + 5);
        this->conv_bias_spinbox->setValue(this->conv_layer->biases[0]);
        this->conv_bias_spinbox->hide();
    }
    this->conv_out_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->conv_out_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
            this->conv_out_texts[r][c] = this->conv_out_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->conv_layer->output[0][r][c], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->conv_out_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
        }
    }
}

void
Cnn_demo::actualize_act_out(void)
{
    unsigned int out_h = this->act_layer->dim_out.height;
    unsigned int out_w = this->act_layer->dim_out.width;
    this->act_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->act_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
            this->act_texts[r][c] = this->act_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->act_layer->output[0][r][c], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->act_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
        }
    }
}

void
Cnn_demo::actualize_pool_out(void)
{
    unsigned int out_h = this->pool_layer->dim_out.height;
    unsigned int out_w = this->pool_layer->dim_out.width;
    this->pool_scene->clear();
    for (unsigned int r = 0; r < out_h; r++) {
        for (unsigned int c = 0; c < out_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->pool_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
            this->pool_texts[r][c] = this->pool_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->pool_layer->output[0][r][c], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->pool_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
        }
    }
}

void
Cnn_demo::actualize_fc_out(void)
{
    unsigned int in_h = this->fc_layer->dim_in.height;
    unsigned int in_w = this->fc_layer->dim_in.width;
    for (unsigned int d = 0; d < this->fc_size; d++) {
        this->fc_weight_scene[d]->clear();
        for (unsigned int r = 0; r < in_h; r++) {
            for (unsigned int c = 0; c < in_w; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                pen.setWidth(2);
                this->fc_weight_scene[d]->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                this->fc_weight_texts[d][r][c] = this->fc_weight_scene[d]->addText(QString::fromStdString(
                        to_string_with_precision(this->fc_layer->weights[d][r * in_w + c], 1)),
                        QFont("Times", 11, QFont::Bold));
                this->fc_weight_texts[d][r][c]->setPos(40 * c + 5, 40 * r + 5);
                this->fc_weight_spinboxes[d][r][c]->setValue(this->fc_layer->weights[d][r * in_w + c]);
                this->fc_weight_spinboxes[d][r][c]->hide();
            }
        }
        this->fc_bias_scene[d]->clear();
        {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->fc_bias_scene[d]->addRect(0, 0, 40, 40, pen, brush);
            this->fc_bias_text[d] = this->fc_bias_scene[d]->addText(QString::fromStdString(
                    to_string_with_precision(this->fc_layer->biases[d], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->fc_bias_text[d]->setPos(0 + 5, 0 + 5);
            this->fc_bias_spinbox[d]->setValue(this->fc_layer->biases[d]);
            this->fc_bias_spinbox[d]->hide();
        }
        this->fc_out_scene[d]->clear();
        {
            QBrush brush(Qt::lightGray);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->fc_out_scene[d]->addRect(0, 0, 60, 40, pen, brush);
            this->fc_out_text[d] = this->fc_out_scene[d]->addText(QString::fromStdString(
                    to_string_with_precision(this->fc_layer->output[d][0][0], 2)),
                    QFont("Times", 11, QFont::Bold));
            this->fc_out_text[d]->setPos(0 + 5, 0 + 5);
        }
    }
}

void
Cnn_demo::on_input_spinBox_editingFinished(void)
{
    unsigned int padding = this->conv_padding;
    unsigned int r = this->current_sb_row;
    unsigned int c = this->current_sb_col;
    this->conv_net->input_layer.input[0][r][c] = this->input_spinboxes[r][c]->value();
    this->input_scene->removeItem(this->input_texts[r][c]);
    this->input_texts[r][c] = this->input_scene->addText(QString::fromStdString(
            to_string_with_precision(this->conv_net->input_layer.input[0][r][c], 1)),
            QFont("Times", 11, QFont::Bold));
    this->input_texts[r][c]->setPos(40 * (c + padding) + 5, 40 * (r + padding) + 5);
    this->input_spinboxes[r][c]->hide();
}

void
Cnn_demo::on_conv_filter_spinBox_editingFinished(void)
{
    unsigned int r = this->current_sb_row;
    unsigned int c = this->current_sb_col;
    this->conv_layer->filters[0][0][r][c] = this->conv_filter_spinboxes[r][c]->value();
    this->conv_filter_scene->removeItem(this->conv_filter_texts[r][c]);
    this->conv_filter_texts[r][c] = this->conv_filter_scene->addText(QString::fromStdString(
            to_string_with_precision(this->conv_layer->filters[0][0][r][c], 1)),
            QFont("Times", 11, QFont::Bold));
    this->conv_filter_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
    this->conv_filter_spinboxes[r][c]->hide();
}

void
Cnn_demo::on_conv_bias_spinBox_editingFinished(void)
{
    this->conv_layer->biases[0] = this->conv_bias_spinbox->value();
    this->conv_bias_scene->removeItem(this->conv_bias_text);
    this->conv_bias_text = this->conv_bias_scene->addText(QString::fromStdString(
            to_string_with_precision(this->conv_layer->biases[0], 1)),
            QFont("Times", 11, QFont::Bold));
    this->conv_bias_text->setPos(0 + 5, 0 + 5);
    this->conv_bias_spinbox->hide();
}

void
Cnn_demo::on_pool_op_comboBox_currentIndexChanged(int index)
{
    this->pool_layer->operation = (index == 0) ?
                Pooling_layer::MAX : Pooling_layer::AVERAGE;
}

void
Cnn_demo::on_fc_weight_spinBox_editingFinished(void)
{
    unsigned int r = this->current_sb_row;
    unsigned int c = this->current_sb_col;
    unsigned int d = this->current_sb_depth;
    unsigned int in_w = this->fc_layer->prev->dim_out.width;
    this->fc_layer->weights[d][r * in_w + c] = this->fc_weight_spinboxes[d][r][c]->value();
    this->fc_weight_scene[d]->removeItem(this->fc_weight_texts[d][r][c]);
    this->fc_weight_texts[d][r][c] = this->fc_weight_scene[d]->addText(QString::fromStdString(
            to_string_with_precision(this->fc_layer->weights[d][r * in_w + c], 1)),
            QFont("Times", 11, QFont::Bold));
    this->fc_weight_texts[d][r][c]->setPos(40 * c + 5, 40 * r + 5);
    this->fc_weight_spinboxes[d][r][c]->hide();
}

void
Cnn_demo::on_fc_bias_spinBox_editingFinished(void)
{
    unsigned int d = this->current_sb_depth;
    this->fc_layer->biases[d] = this->fc_bias_spinbox[d]->value();
    this->fc_bias_scene[d]->removeItem(this->fc_bias_text[d]);
    this->fc_bias_text[d] = this->fc_bias_scene[d]->addText(QString::fromStdString(
            to_string_with_precision(this->fc_layer->biases[d], 1)),
            QFont("Times", 11, QFont::Bold));
    this->fc_bias_text[d]->setPos(0 + 5, 0 + 5);
    this->fc_bias_spinbox[d]->hide();
}

void
Cnn_demo::on_learning_rate_spinBox_editingFinished(void)
{
    if (this->conv_net != nullptr) {
        this->conv_net->set_mi(this->ui->doubleSpinBox_learning_rate->value());
    }
}

void Cnn_demo::on_actionInit_triggered()
{
    this->init_input();
    this->init_conv();
    this->init_act();
    this->init_pool();
    this->init_fc();
    this->current_layer = &(this->conv_net->input_layer);
    this->current_output = Dimensions{0, 0, 0};
    this->conv_net->feed_forward();
    this->disable_backprop();
    this->enable_forward();
    this->backprop_started = false;
}

void Cnn_demo::on_actionRun_triggered()
{
    this->conv_net->feed_forward();
    this->actualize_input();
    this->actualize_conv_out();
    this->actualize_act_out();
    this->actualize_pool_out();
    this->actualize_fc_out();
    this->current_layer = &(this->conv_net->input_layer);
    this->current_output = Dimensions{0, 0, 0};
    this->enable_backprop();
}

void Cnn_demo::on_actionStep_triggered()
{
    if (this->current_layer == &(this->conv_net->input_layer)) {
        this->conv_net->feed_forward();
        this->actualize_input(my_red);
        this->actualize_conv_out();
        this->actualize_act_out();
        this->actualize_pool_out();
        this->actualize_fc_out();
        this->reset_conv_out();
        this->reset_act_out();
        this->reset_pool_out();
        this->reset_fc_out();
        this->current_layer = this->conv_layer;
        this->current_output = Dimensions{0, 0, 0};
        this->disable_backprop();
    } else if (this->current_layer == this->conv_layer) {
        unsigned int out_h = this->conv_layer->dim_out.height;
        unsigned int out_w = this->conv_layer->dim_out.width;
        unsigned int filter_h = this->conv_filter_h;
        unsigned int filter_w = this->conv_filter_w;
        unsigned int &ch = this->current_output.height;
        unsigned int &cw = this->current_output.width;
        unsigned int size = this->input_size;
        unsigned int padding = this->conv_padding;
        unsigned int stride = this->conv_stride;
        this->actualize_input();
        this->input_scene->clear();
        for (unsigned int r = 0; r < size + 2 * padding; r++) {
            for (unsigned int c = 0; c < size + 2 * padding; c++) {
                if (r < padding || c < padding || r >= padding + size || c >= padding + size) {
                    QBrush brush(Qt::white);
                    QPen pen(Qt::gray);
                    if (r >= ch * stride && r < ch * stride + filter_h &&
                            c >= cw * stride && c < cw * stride + filter_w) {
                        brush.setColor(my_green);
                    }
                    pen.setWidth(1);
                    pen.setStyle(Qt::DashLine);
                    this->input_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                    QGraphicsTextItem *text = this->input_scene->addText(QString::fromStdString(
                            to_string_with_precision(this->conv_layer->padding_val, 1)),
                            QFont("Times", 11, QFont::Bold));
                    text->setPos(40 * c + 5, 40 * r + 5);
                    text->setDefaultTextColor(Qt::gray);
                }
            }
        }
        for (unsigned int r = 0; r < size + 2 * padding; r++) {
            for (unsigned int c = 0; c < size + 2 * padding; c++) {
                if (!(r < padding || c < padding || r >= padding + size || c >= padding + size)) {
                    QBrush brush(Qt::white);
                    QPen pen(Qt::black);
                    if (r >= ch * stride && r < ch * stride + filter_h &&
                            c >= cw * stride && c < cw * stride + filter_w) {
                        brush.setColor(my_green);
                    }
                    pen.setWidth(2);
                    this->input_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                    this->input_texts[r - padding][c - padding] = this->input_scene->addText(QString::fromStdString(
                            to_string_with_precision(this->conv_net->input_layer.input[0][r - padding][c - padding], 1)),
                            QFont("Times", 11, QFont::Bold));
                    this->input_texts[r - padding][c - padding]->setPos(40 * c + 5, 40 * r + 5);
                }
            }
        }
        this->conv_filter_scene->clear();
        for (unsigned int r = 0; r < filter_h; r++) {
            for (unsigned int c = 0; c < filter_w; c++) {
                QBrush brush(my_blue);
                QPen pen(Qt::black);
                pen.setWidth(2);
                this->conv_filter_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                this->conv_filter_texts[r][c] = this->conv_filter_scene->addText(QString::fromStdString(
                        to_string_with_precision(this->conv_layer->filters[0][0][r][c], 1)),
                        QFont("Times", 11, QFont::Bold));
                this->conv_filter_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
            }
        }
        this->conv_bias_scene->clear();
        {
            QBrush brush(my_blue);
            QPen pen(Qt::black);
            pen.setWidth(2);
            this->conv_bias_scene->addRect(0, 0, 40, 40, pen, brush);
            this->conv_bias_text = this->conv_bias_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->conv_layer->biases[0], 1)),
                    QFont("Times", 11, QFont::Bold));
            this->conv_bias_text->setPos(0 + 5, 0 + 5);
        }
        this->conv_out_scene->clear();
        for (unsigned int r = 0; r < out_h; r++) {
            for (unsigned int c = 0; c < out_w; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                if (r == ch && c == cw) {
                    brush.setColor(my_red);
                }
                pen.setWidth(2);
                this->conv_out_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                if (r < ch || (r == ch && c <= cw)) {
                    this->conv_out_texts[r][c] = this->conv_out_scene->addText(QString::fromStdString(
                            to_string_with_precision(this->conv_layer->output[0][r][c], 1)),
                            QFont("Times", 11, QFont::Bold));
                    this->conv_out_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
                }
            }
        }
        if (cw < out_w - 1) {
            cw++;
        } else if (ch < out_h - 1) {
            cw = 0;
            ch++;
        } else {
            this->current_layer = this->act_layer;
            ch = 0;
            cw = 0;
        }
    } else if (this->current_layer == this->act_layer) {
        unsigned int out_h = this->act_layer->dim_out.height;
        unsigned int out_w = this->act_layer->dim_out.width;
        unsigned int &ch = this->current_output.height;
        unsigned int &cw = this->current_output.width;
        if (ch == 0 && cw == 0) {
            this->actualize_input();
            this->actualize_conv_out();
        }
        this->conv_out_scene->clear();
        for (unsigned int r = 0; r < out_h; r++) {
            for (unsigned int c = 0; c < out_w; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                if (r == ch && c == cw) {
                    brush.setColor(my_green);
                }
                pen.setWidth(2);
                this->conv_out_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                this->conv_out_texts[r][c] = this->conv_out_scene->addText(QString::fromStdString(
                        to_string_with_precision(this->conv_layer->output[0][r][c], 1)),
                        QFont("Times", 11, QFont::Bold));
                this->conv_out_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
            }
        }
        this->act_scene->clear();
        for (unsigned int r = 0; r < out_h; r++) {
            for (unsigned int c = 0; c < out_w; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                if (r == ch && c == cw) {
                    brush.setColor(my_red);
                }
                pen.setWidth(2);
                this->act_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                if (r < ch || (r == ch && c <= cw)) {
                    this->act_texts[r][c] = this->act_scene->addText(QString::fromStdString(
                            to_string_with_precision(this->act_layer->output[0][r][c], 1)),
                            QFont("Times", 11, QFont::Bold));
                    this->act_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
                }
            }
        }
        if (cw < out_w - 1) {
            cw++;
        } else if (ch < out_h - 1) {
            cw = 0;
            ch++;
        } else {
            this->current_layer = this->pool_layer;
            ch = 0;
            cw = 0;
        }
    } else if (this->current_layer == this->pool_layer) {
        unsigned int out_h = this->pool_layer->dim_out.height;
        unsigned int out_w = this->pool_layer->dim_out.width;
        unsigned int in_h = this->pool_layer->dim_in.height;
        unsigned int in_w = this->pool_layer->dim_in.width;
        unsigned int &ch = this->current_output.height;
        unsigned int &cw = this->current_output.width;
        unsigned int stride = this->pool_stride;
        unsigned int ph = this->pool_height;
        unsigned int pw = this->pool_width;
        if (ch == 0 && cw == 0) {
            this->actualize_conv_out();
            this->actualize_act_out();
        }
        this->act_scene->clear();
        for (unsigned int r = 0; r < in_h; r++) {
            for (unsigned int c = 0; c < in_w; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                if (r >= ch * stride && r < ch * stride + ph &&
                        c >= cw * stride && c < cw * stride + pw) {
                    brush.setColor(my_green);
                }
                pen.setWidth(2);
                this->act_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                this->act_texts[r][c] = this->act_scene->addText(QString::fromStdString(
                        to_string_with_precision(this->act_layer->output[0][r][c], 1)),
                        QFont("Times", 11, QFont::Bold));
                this->act_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
            }
        }
        this->pool_scene->clear();
        for (unsigned int r = 0; r < out_h; r++) {
            for (unsigned int c = 0; c < out_w; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                if (r == ch && c == cw) {
                    brush.setColor(my_red);
                }
                pen.setWidth(2);
                this->pool_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                if (r < ch || (r == ch && c <= cw)) {
                    this->pool_texts[r][c] = this->pool_scene->addText(QString::fromStdString(
                        to_string_with_precision(this->pool_layer->output[0][r][c], 1)),
                        QFont("Times", 11, QFont::Bold));
                    this->pool_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
                }
            }
        }
        if (cw < out_w - 1) {
            cw++;
        } else if (ch < out_h - 1) {
            cw = 0;
            ch++;
        } else {
            this->current_layer = this->fc_layer;
            ch = 0;
            cw = 0;
        }
    } else if (this->current_layer == this->fc_layer) {
        unsigned int &cd = this->current_output.depth;
        unsigned int in_h = this->fc_layer->dim_in.height;
        unsigned int in_w = this->fc_layer->dim_in.width;
        if (cd == 0) {
            this->actualize_act_out();
            this->actualize_pool_out();
        }
        for (unsigned int r = 0; r < in_h; r++) {
            for (unsigned int c = 0; c < in_w; c++) {
                QBrush brush(my_green);
                QPen pen(Qt::black);
                pen.setWidth(2);
                this->pool_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                this->pool_texts[r][c] = this->pool_scene->addText(QString::fromStdString(
                        to_string_with_precision(this->pool_layer->output[0][r][c], 1)),
                        QFont("Times", 11, QFont::Bold));
                this->pool_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
            }
        }
        for (unsigned int d = 0; d < this->fc_size; d++) {
            this->fc_weight_scene[d]->clear();
            for (unsigned int r = 0; r < in_h; r++) {
                for (unsigned int c = 0; c < in_w; c++) {
                    QBrush brush(Qt::white);
                    QPen pen(Qt::black);
                    if (d == cd) {
                        brush.setColor(my_blue);
                    }
                    pen.setWidth(2);
                    this->fc_weight_scene[d]->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                    this->fc_weight_texts[d][r][c] = this->fc_weight_scene[d]->addText(QString::fromStdString(
                            to_string_with_precision(this->fc_layer->weights[d][r * in_w + c], 1)),
                            QFont("Times", 11, QFont::Bold));
                    this->fc_weight_texts[d][r][c]->setPos(40 * c + 5, 40 * r + 5);
                }
            }
            this->fc_bias_scene[d]->clear();
            {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                if (d == cd) {
                    brush.setColor(my_blue);
                }
                pen.setWidth(2);
                this->fc_bias_scene[d]->addRect(0, 0, 40, 40, pen, brush);
                this->fc_bias_text[d] = this->fc_bias_scene[d]->addText(QString::fromStdString(
                        to_string_with_precision(this->fc_layer->biases[d], 1)),
                        QFont("Times", 11, QFont::Bold));
                this->fc_bias_text[d]->setPos(0 + 5, 0 + 5);
            }
            this->fc_out_scene[d]->clear();
            {
                QBrush brush(Qt::lightGray);
                QPen pen(Qt::black);
                if (d == cd) {
                    brush.setColor(my_red);
                }
                pen.setWidth(2);
                this->fc_out_scene[d]->addRect(0, 0, 60, 40, pen, brush);
                if (d <= cd) {
                    this->fc_out_text[d] = this->fc_out_scene[d]->addText(QString::fromStdString(
                            to_string_with_precision(this->fc_layer->output[d][0][0], 2)),
                            QFont("Times", 11, QFont::Bold));
                    this->fc_out_text[d]->setPos(0 + 5, 0 + 5);
                }
            }
        }
        if (cd < this->fc_size - 1) {
            cd++;
        } else {
            this->current_layer = &(this->conv_net->input_layer);
            this->current_output = Dimensions{0, 0, 0};
            this->enable_backprop();
        }
    }
}

void
Cnn_demo::on_actionBackprop_Run_triggered()
{
    if (this->backprop_started == false) {
        vector<outval_t> expected_values;
        for (auto &a: this->expected_values_spinboxes) {
            expected_values.push_back(a->value());
        }
        this->conv_net->backprop(expected_values);
    }
    this->disable_backprop();
    this->actualize_input();
    this->actualize_conv_out();
    this->actualize_act_out();
    this->actualize_pool_out();
    this->actualize_fc_out();
    this->reset_conv_out();
    this->reset_act_out();
    this->reset_pool_out();
    this->reset_fc_out();
    this->enable_forward();
}

void
Cnn_demo::paint_fc_weight(bool set, QColor color, Dimensions dim)
{
    unsigned int in_h = this->fc_layer->dim_in.height;
    unsigned int in_w = this->fc_layer->dim_in.width;
    for (unsigned int d = 0; d < this->fc_size; d++) {
        this->fc_weight_scene[d]->clear();
        for (unsigned int r = 0; r < in_h; r++) {
            for (unsigned int c = 0; c < in_w; c++) {
                QBrush brush(Qt::white);
                QPen pen(Qt::black);
                if (set == true && d == dim.depth && r == dim.height && c == dim.width) {
                    brush.setColor(color);
                    this->fc_weight_spinboxes[d][r][c]->setValue(this->fc_layer->weights[d][r * in_w + c]);
                }
                pen.setWidth(2);
                this->fc_weight_scene[d]->addRect(40 * c, 40 * r, 40, 40, pen, brush);
                this->fc_weight_texts[d][r][c] = this->fc_weight_scene[d]->addText(QString::fromStdString(
                        to_string_with_precision(this->fc_weight_spinboxes[d][r][c]->value(), 1)),
                        QFont("Times", 11, QFont::Bold));
                this->fc_weight_texts[d][r][c]->setPos(40 * c + 5, 40 * r + 5);
            }
        }
    }
}

void
Cnn_demo::paint_fc_bias(bool set, QColor color, Dimensions dim)
{
    for (unsigned int d = 0; d < this->fc_size; d++) {
        this->fc_bias_scene[d]->clear();
        QBrush brush(Qt::white);
        QPen pen(Qt::black);
        if (set == true && d == dim.depth) {
            brush.setColor(color);
            this->fc_bias_spinbox[d]->setValue(this->fc_layer->biases[d]);
        }
        pen.setWidth(2);
        this->fc_bias_scene[d]->addRect(0, 0, 40, 40, pen, brush);
        this->fc_bias_text[d] = this->fc_bias_scene[d]->addText(QString::fromStdString(
                to_string_with_precision(this->fc_bias_spinbox[d]->value(), 1)),
                QFont("Times", 11, QFont::Bold));
        this->fc_bias_text[d]->setPos(0 + 5, 0 + 5);
    }
}

void
Cnn_demo::paint_conv_filter(bool set, QColor color, Dimensions dim)
{
    unsigned int filter_h = this->conv_filter_h;
    unsigned int filter_w = this->conv_filter_w;
    this->conv_filter_scene->clear();
    for (unsigned int r = 0; r < filter_h; r++) {
        for (unsigned int c = 0; c < filter_w; c++) {
            QBrush brush(Qt::white);
            QPen pen(Qt::black);
            if (set == true && r == dim.height && c == dim.width) {
                brush.setColor(color);
                this->conv_filter_spinboxes[r][c]->setValue(this->conv_layer->filters[0][0][r][c]);
            }
            pen.setWidth(2);
            this->conv_filter_scene->addRect(40 * c, 40 * r, 40, 40, pen, brush);
            this->conv_filter_texts[r][c] = this->conv_filter_scene->addText(QString::fromStdString(
                    to_string_with_precision(this->conv_filter_spinboxes[r][c]->value(), 1)),
                    QFont("Times", 11, QFont::Bold));
            this->conv_filter_texts[r][c]->setPos(40 * c + 5, 40 * r + 5);
        }
    }
}

void
Cnn_demo::paint_conv_bias(bool set, QColor color, Dimensions dim)
{
    (void)dim;
    this->conv_bias_scene->clear();
    QBrush brush(Qt::white);
    QPen pen(Qt::black);
    if (set == true) {
        brush.setColor(color);
        this->conv_bias_spinbox->setValue(this->conv_layer->biases[0]);
    }
    pen.setWidth(2);
    this->conv_bias_scene->addRect(0, 0, 40, 40, pen, brush);
    this->conv_bias_text = this->conv_bias_scene->addText(QString::fromStdString(
            to_string_with_precision(this->conv_bias_spinbox->value(), 1)),
            QFont("Times", 11, QFont::Bold));
    this->conv_bias_text->setPos(0 + 5, 0 + 5);
}

void
Cnn_demo::on_actionBackprop_Step_triggered()
{
    if (this->backprop_started == false) {
        this->actualize_input();
        this->actualize_conv_out();
        this->actualize_act_out();
        this->actualize_pool_out();
        this->actualize_fc_out();
        vector<outval_t> expected_values;
        for (auto &a: this->expected_values_spinboxes) {
            expected_values.push_back(a->value());
        }
        this->conv_net->backprop(expected_values);
        this->backprop_started = true;
        this->disable_forward();
    }

    unsigned int &cd = this->backprop_current_dim.depth;
    unsigned int &ch = this->backprop_current_dim.height;
    unsigned int &cw = this->backprop_current_dim.width;
    unsigned int &fcd = this->fc_layer->dim_out.depth;
    unsigned int &fch = this->fc_layer->dim_in.height;
    unsigned int &fcw = this->fc_layer->dim_in.width;
    unsigned int &conv_fh = this->conv_layer->filter_height;
    unsigned int &conv_fw = this->conv_layer->filter_width;

    switch (this->backprop_current_item) {
    case FC_WEIGHT:
        this->paint_fc_weight(true, my_blue, this->backprop_current_dim);
        this->paint_fc_bias();
        this->paint_conv_filter();
        this->paint_conv_bias();
        if (cw < fcw - 1) {
            cw++;
        } else if (ch < fch - 1) {
            cw = 0;
            ch++;
        } else {
            this->backprop_current_item = FC_BIAS;
            cw = 0;
            ch = 0;
        }
        break;
    case FC_BIAS:
        this->paint_fc_weight();
        this->paint_fc_bias(true, my_blue, this->backprop_current_dim);
        this->paint_conv_filter();
        this->paint_conv_bias();
        if (cd < fcd - 1) {
            cw = 0;
            ch = 0;
            cd++;
            this->backprop_current_item = FC_WEIGHT;
        } else {
            this->backprop_current_item = CONV_FILTER;
            cw = 0;
            ch = 0;
            cd = 0;
        }
        break;
    case CONV_FILTER:
        this->paint_fc_weight();
        this->paint_fc_bias();
        this->paint_conv_filter(true, my_blue, this->backprop_current_dim);
        this->paint_conv_bias();
        if (cw < conv_fw - 1) {
            cw++;
        } else if (ch < conv_fh - 1) {
            cw = 0;
            ch++;
        } else {
            this->backprop_current_item = CONV_BIAS;
            cw = 0;
            ch = 0;
        }
        break;
    case CONV_BIAS:
        if (cw == 0) {
            this->paint_fc_weight();
            this->paint_fc_bias();
            this->paint_conv_filter();
            this->paint_conv_bias(true, my_blue, this->backprop_current_dim);
            cw++;
        } else {
            this->backprop_started = false;
            this->disable_backprop();
            this->actualize_input();
            this->actualize_conv_out();
            this->actualize_act_out();
            this->actualize_pool_out();
            this->actualize_fc_out();
            this->reset_conv_out();
            this->reset_act_out();
            this->reset_pool_out();
            this->reset_fc_out();
            this->enable_forward();
        }
        break;
    }
}
