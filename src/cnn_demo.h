/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef CNN_DEMO_H
#define CNN_DEMO_H

/* System and standard library headers. */
#include <vector>

/* Qt libraries headers. */
#include <QMainWindow>
#include <QGraphicsView>
#include <QGridLayout>
#include <QSpinBox>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include <QList>

/* Own headers. */
#include "conv_net.h"
#include "conv_layer.h"
#include "activation_layer.h"
#include "pooling_layer.h"
#include "fc_layer.h"

using namespace std;

namespace Ui {
class Cnn_demo;
class GraphicsScene;
}

class Cnn_demo;

class GraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit GraphicsScene(QObject *parent = 0, Cnn_demo *cd = nullptr);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent);
signals:

public slots:

private:
    Cnn_demo *cnn_demo;
};

class Cnn_demo : public QMainWindow
{
    Q_OBJECT

public:
    explicit Cnn_demo(QWidget *parent = 0);
    ~Cnn_demo();

private:
    void init_backprop(void);
    void disable_backprop(void);
    void enable_backprop(void);
    void disable_forward(void);
    void enable_forward(void);
    void init_input(void);
    void init_conv(void);
    void init_act(void);
    void init_pool(void);
    void init_fc(void);

    void actualize_input(QColor input_color = Qt::white);
    void actualize_conv_out(void);
    void actualize_act_out(void);
    void actualize_pool_out(void);
    void actualize_fc_out(void);

    void reset_conv_out(void);
    void reset_act_out(void);
    void reset_pool_out(void);
    void reset_fc_out(void);

    void paint_fc_weight(bool set = false, QColor color = Qt::white,
            Dimensions dim = Dimensions{0, 0, 0});
    void paint_fc_bias(bool set = false, QColor color = Qt::white,
            Dimensions dim = Dimensions{0, 0, 0});
    void paint_conv_filter(bool set = false, QColor color = Qt::white,
            Dimensions dim = Dimensions{0, 0, 0});
    void paint_conv_bias(bool set = false, QColor color = Qt::white,
            Dimensions dim = Dimensions{0, 0, 0});

private slots:
    void on_input_spinBox_editingFinished(void);
    void on_conv_filter_spinBox_editingFinished(void);
    void on_conv_bias_spinBox_editingFinished(void);
    void on_pool_op_comboBox_currentIndexChanged(int index);
    void on_fc_weight_spinBox_editingFinished(void);
    void on_fc_bias_spinBox_editingFinished(void);
    void on_learning_rate_spinBox_editingFinished(void);

    void on_actionInit_triggered();
    void on_actionRun_triggered();

    void on_actionStep_triggered();

    void on_actionBackprop_Run_triggered();

    void on_actionBackprop_Step_triggered();

public:
    Ui::Cnn_demo *ui;
    unsigned int input_size;
    unsigned int conv_padding;
    unsigned int conv_stride;
    unsigned int conv_filter_h;
    unsigned int conv_filter_w;
    unsigned int pool_stride;
    unsigned int pool_height;
    unsigned int pool_width;
    unsigned int fc_size;

    Conv_net *conv_net;
    Conv_layer *conv_layer;
    Activation_layer *act_layer;
    Pooling_layer *pool_layer;
    Fc_layer *fc_layer;

    QGraphicsView *input_view;
    GraphicsScene *input_scene;
    vector<vector<QGraphicsTextItem *>> input_texts;
    vector<vector<QDoubleSpinBox *>> input_spinboxes;
    unsigned int current_sb_row;
    unsigned int current_sb_col;
    unsigned int current_sb_depth;

    QGraphicsView *conv_out_view;
    QGraphicsView *conv_filter_view;
    QGraphicsView *conv_bias_view;
    GraphicsScene *conv_out_scene;
    GraphicsScene *conv_filter_scene;
    GraphicsScene *conv_bias_scene;
    vector<vector<QGraphicsTextItem *>> conv_out_texts;
    vector<vector<QGraphicsTextItem *>> conv_filter_texts;
    QGraphicsTextItem *conv_bias_text;
    vector<vector<QDoubleSpinBox *>> conv_filter_spinboxes;
    QDoubleSpinBox *conv_bias_spinbox;

    QGraphicsView *act_view;
    GraphicsScene *act_scene;
    vector<vector<QGraphicsTextItem *>> act_texts;

    QGraphicsView *pool_view;
    GraphicsScene *pool_scene;
    vector<vector<QGraphicsTextItem *>> pool_texts;

    vector<QGraphicsView *> fc_weight_view;
    vector<QGraphicsView *> fc_bias_view;
    vector<QGraphicsView *> fc_out_view;
    vector<GraphicsScene *> fc_weight_scene;
    vector<GraphicsScene *> fc_bias_scene;
    vector<GraphicsScene *> fc_out_scene;
    vector<vector<vector<QGraphicsTextItem *>>> fc_weight_texts;
    vector<QGraphicsTextItem *> fc_bias_text;
    vector<QGraphicsTextItem *> fc_out_text;
    vector<vector<vector<QDoubleSpinBox *>>> fc_weight_spinboxes;
    vector<QDoubleSpinBox *> fc_bias_spinbox;
    vector<QFrame *> fc_lines;

    vector<QDoubleSpinBox *> expected_values_spinboxes;

    Layer *current_layer;
    Dimensions current_output;

    bool backprop_started;

    enum backprop_current {
        FC_WEIGHT,
        FC_BIAS,
        CONV_FILTER,
        CONV_BIAS,
    };
    enum backprop_current backprop_current_item;
    Dimensions backprop_current_dim;
};

#endif // CNN_DEMO_H
