#-------------------------------------------------
#
# Project created by QtCreator 2017-11-25T16:40:56
#
#-------------------------------------------------
# SFC 2017/2018 Demonstration of Convolutional Neural Network activity
# Author: Matej Vido, xvidom00@stud.fit.vutbr.cz
# Date:   2017/11

QT += core gui
CONFIG += c++11
CONFIG += warn_off
QMAKE_CXXFLAGS += -std=c++11
#QMAKE_CXXFLAGS += -Wall -Wextra -pedantic -g -DCNN_DEBUG

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cnn_demo
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    gui_main.cpp \
    cnn_demo.cpp \
    activation_layer.cpp \
    conv_layer.cpp \
    conv_net.cpp \
    fc_layer.cpp \
    input_layer.cpp \
    layer.cpp \
    pooling_layer.cpp

HEADERS += \
    cnn_demo.h \
    activation_layer.h \
    conv_layer.h \
    conv_net.h \
    debug.h \
    fc_layer.h \
    input_layer.h \
    pooling_layer.h \
    layer.h

FORMS += \
    cnn_demo.ui

MOC_DIR = ./build
OBJECTS_DIR = ./build
RCC_DIR = ./build
UI_DIR = ./build
