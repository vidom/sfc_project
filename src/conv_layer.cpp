/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <iostream>
#include <cstdlib>
#include <vector>

/* Own headers. */
#include "debug.h"
#include "layer.h"
#include "conv_layer.h"

using namespace std;

Conv_layer::Conv_layer(Dimensions di, unsigned int fcount, unsigned int fh,
            unsigned int fw, unsigned int s, unsigned int p, outval_t pval,
            vector<filter_t> _filters, vector<bias_t> _biases) :
    filter_count{fcount},
    filter_depth{di.depth},
    filter_height{fh},
    filter_width{fw},
    stride{s},
    padding{p},
    padding_val{pval},
    filters{_filters},
    biases{_biases}
{
    this->prev = nullptr;
    this->next = nullptr;
    this->dim_in = di;

    if ((di.width + 2 * p < fw) || ((di.width - fw + 2 * p) % s != 0) ||
            (di.height + 2 * p < fh) || ((di.height - fh + 2 * p) % s != 0) ||
            fh == 0 || fw == 0 || s == 0 || di.depth == 0 || fcount == 0) {
        throw Layer_exception{"Incompatible parameters for convolutional layer!"};
    }

    if (this->filters.size() != this->filter_count ||
            this->biases.size() != this->filter_count) {
        throw Layer_exception{"Incompatible filters and biases for convolutional layer!"};
    }

    for (unsigned int i = 0; i < this->filter_count; i++) {
        unsigned int depth = this->filters[i].size();
        unsigned int height = 0;
        unsigned int width = 0;
        if (depth > 0) {
            height = this->filters[i][0].size();
            if (height > 0) {
                width = this->filters[i][0][0].size();
            }
            for (auto &a: this->filters[i]) {
                if (a.size() != height) {
                    throw Layer_exception{"Incompatible dimensions of filter!"};
                }
                for (auto &b: a) {
                    if (b.size() != width) {
                        throw Layer_exception{"Incompatible dimensions of filter!"};
                    }
                }
            }
        }
        if (depth == 0) {
            throw Layer_exception{"Filter depth must be greater than 0!"};
        }
        if (height == 0) {
            throw Layer_exception{"Filter height must be greater than 0!"};
        }
        if (width == 0) {
            throw Layer_exception{"Filter width must be greater than 0!"};
        }
        if (depth != this->filter_depth || height != this->filter_height ||
                width != this->filter_width) {
            throw Layer_exception{"Incompatible dimensions of filter!"};
        }
    }

    this->dim_out.depth = this->filter_count;
    this->dim_out.height = ((di.height - fh + 2 * p) / s) + 1;
    this->dim_out.width = ((di.width - fw + 2 * p) / s) + 1;
    this->output = vector< vector< vector<outval_t> > >(
        this->dim_out.depth, vector< vector<outval_t> >(
            this->dim_out.height, vector<outval_t>(this->dim_out.width)));

    CNN_DEBUG_MSG("Conv_layer is created!");
}

Conv_layer::Conv_layer(Dimensions di, unsigned int fcount, unsigned int fh,
            unsigned int fw, unsigned int s, unsigned int p, outval_t pval) :
    filter_count{fcount},
    filter_depth{di.depth},
    filter_height{fh},
    filter_width{fw},
    stride{s},
    padding{p},
    padding_val{pval}
{
    this->prev = nullptr;
    this->next = nullptr;
    this->dim_in = di;

    if ((di.width + 2 * p < fw) || ((di.width - fw + 2 * p) % s != 0) ||
            (di.height + 2 * p < fh) || ((di.height - fh + 2 * p) % s != 0) ||
            fh == 0 || fw == 0 || s == 0 || di.depth == 0 || fcount == 0) {
        throw Layer_exception{"Incompatible parameters for convolutional layer!"};
    }

    srand(42);

    this->filters = vector<filter_t>(this->filter_count,
        vector<vector<vector<outval_t>>>(this->filter_depth,
            vector<vector<outval_t>>(this->filter_height,
            vector<outval_t>(this->filter_width))));
    this->biases = vector<outval_t>(this->filter_count);

    for (auto &f: this->filters) {
        for (auto &d: f) {
            for (auto &h: d) {
                for (auto &w: h) {
                    w = ((outval_t)rand()) / (RAND_MAX);
                }
            }
        }
    }

    for (auto &b: this->biases) {
        b = ((outval_t)rand()) / (RAND_MAX);
    }

    this->dim_out.depth = this->filter_count;
    this->dim_out.height = ((di.height - fh + 2 * p) / s) + 1;
    this->dim_out.width = ((di.width - fw + 2 * p) / s) + 1;
    this->output = vector< vector< vector<outval_t> > >(
        this->dim_out.depth, vector< vector<outval_t> >(
            this->dim_out.height, vector<outval_t>(this->dim_out.width)));

    CNN_DEBUG_MSG("Conv_layer is created!");
}

Conv_layer::~Conv_layer(void)
{
    CNN_DEBUG_MSG("Conv_layer is destroyed!");
}

outval_t
Conv_layer::get_input(unsigned int d, unsigned int h, unsigned int w)
{
    if (this->padding == 0) {
        return this->prev->output[d][h][w];
    } else if (h < this->padding || h >= this->padding + this->dim_in.height ||
            w < this->padding || w >= this->padding + this->dim_in.width) {
        return this->padding_val;
    } else {
        return this->prev->output[d][h - this->padding][w - this->padding];
    }
}

outval_t
Conv_layer::get_neuron_output(unsigned int d,
        unsigned int h, unsigned int w)
{
    outval_t out_value = 0;
    unsigned int fd;
    unsigned int fh;
    unsigned int fw;
    for (fd = 0; fd < this->filter_depth; fd++) {
        for (fh = 0; fh < this->filter_height; fh++) {
            for (fw = 0; fw < this->filter_width; fw++) {
                out_value += this->get_input(fd, (h * this->stride) + fh,
                        (w * this->stride) + fw) * this->filters[d][fd][fh][fw];
            }
        }
    }
    out_value += this->biases[d];
    return out_value;
}

void
Conv_layer::backprop(double mi, vector<vector<vector<double> > > deltas)
{
    /* Modify filters */
    for (unsigned int fcount = 0; fcount < this->filter_count; fcount++) {
        for (unsigned int fd = 0; fd < this->filter_depth; fd++) {
            for (unsigned int fh = 0; fh < this->filter_height; fh++) {
                for (unsigned int fw = 0; fw < this->filter_width; fw++) {
                    double delta_w = 0;
                    unsigned int count = 0;
                    for (unsigned int d = 0; d < this->dim_out.depth; d++) {
                        for (unsigned int h = 0; h < this->dim_out.height; h++) {
                            for (unsigned int w = 0; w < this->dim_out.width; w++) {
                                delta_w = delta_w + mi * deltas[d][h][w] * this->get_input(
                                        fd, (h * this->stride) + fh, ((w * this->stride) + fw) *
                                        this->filters[d][fd][fh][fw]);
                                count++;
                            }
                        }
                    }
                    delta_w = delta_w / count;
                    //cerr << delta_w << endl;
                    this->filters[fcount][fd][fh][fw] = this->filters[fcount][fd][fh][fw] + delta_w;
                }
            }
        }
    }
    /* Modify biases */
    for (unsigned int fcount = 0; fcount < this->filter_count; fcount++) {
        double delta_w = 0;
        unsigned int count = 0;
        for (unsigned int d = 0; d < this->dim_out.depth; d++) {
            for (unsigned int h = 0; h < this->dim_out.height; h++) {
                for (unsigned int w = 0; w < this->dim_out.width; w++) {
                    delta_w = delta_w + mi * deltas[d][h][w];
                    count++;
                }
            }
        }
        delta_w = delta_w / count;
        //cerr << delta_w << endl;
        this->biases[fcount] = this->biases[fcount] + delta_w;
    }
}

void
Conv_layer::print(ostream &stream)
{
    stream << "--Convolutional Layer--" << endl;
    this->print_dim(stream);
    this->print_params(stream);
}

void
Conv_layer::print_params(ostream &stream)
{
    stream << "  Filter count:" << endl;
    stream << "    " << filter_count << endl;
    stream << "  Filter depth:" << endl;
    stream << "    " << filter_depth << endl;
    stream << "  Filter height:" << endl;
    stream << "    " << filter_height << endl;
    stream << "  Filter width:" << endl;
    stream << "    " << filter_width << endl;
    stream << "  Stride:" << endl;
    stream << "    " << stride << endl;
    stream << "  Padding:" << endl;
    stream << "    " << padding << endl;
    stream << "  Padding value:" << endl;
    stream << "    " << padding_val << endl;
    for (unsigned int f = 0; f < this->filters.size(); f++) {
        stream << "Filter " << f << ":" << endl;
        for (unsigned int d = 0; d < this->filters[f].size(); d++) {
            for (unsigned int h = 0; h < this->filters[f][d].size(); h++) {
                for (unsigned int w = 0; w < this->filters[f][d][h].size(); w++) {
                    stream << this->filters[f][d][h][w];
                    if (w < this->filters[f][d][h].size() - 1) {
                        stream << " ";
                    }
                }
                stream << endl;
            }
            if (d < this->filters[f].size() - 1) {
                stream << endl;
            }
        }
        stream << endl;
    }
    stream << "Biases:" << endl;
    for (unsigned int b = 0; b < this->biases.size(); b++) {
        stream << this->biases[b] << endl;
    }
    stream << endl;
}
