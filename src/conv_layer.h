/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef __CONV_LAYER_H__
#define __CONV_LAYER_H__

/* System and standard library headers. */
#include <iostream>
#include <vector>

/* Own headers. */
#include "layer.h"

using namespace std;

typedef vector<vector<vector<outval_t>>> filter_t;
typedef outval_t bias_t;

/**
 * @brief Convolutional Layer
 */
class Conv_layer: public Layer {
public:
    unsigned int filter_count;
    unsigned int filter_depth;
    unsigned int filter_height;
    unsigned int filter_width;
    unsigned int stride;
    unsigned int padding;
    outval_t padding_val;
    vector<filter_t> filters;
    vector<bias_t> biases;

    /**
     * @brief Constructor for initialization of filters and biases
     *        with random values in range <0,1).
     */
    Conv_layer(Dimensions di, unsigned int fcount, unsigned int fh,
            unsigned int fw, unsigned int s, unsigned int p, outval_t pval);
    /**
     * @brief Constructor for initialization of filters and biases
     *        with specified values.
     */
    Conv_layer(Dimensions di, unsigned int fcount, unsigned int fh,
            unsigned int fw, unsigned int s, unsigned int p, outval_t pval,
            vector<filter_t> _filters, vector<bias_t> _biases);
    ~Conv_layer(void);

    outval_t get_input(unsigned int d, unsigned int h, unsigned int w);
    outval_t get_neuron_output(unsigned int d, unsigned int h, unsigned int w);
    void backprop(double mi, vector<vector<vector<double>>> deltas);

    void print(ostream &stream);
    void print_params(ostream &stream);
};

#endif /* __CONV_LAYER_H__ */
