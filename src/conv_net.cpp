/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <iostream>
#include <utility>

/* Own headers. */
#include "debug.h"
#include "conv_net.h"
#include "layer.h"

using namespace std;

Conv_net::Conv_net(Dimensions dim_in, double _mi):
    input_layer{dim_in},
    last_layer{&input_layer},
    mi{_mi}
{
    CNN_DEBUG_MSG("Conv_net is created!");
}

Conv_net::~Conv_net(void)
{
    while (this->last_layer != &(this->input_layer)) {
        Layer *tmp = this->last_layer;
        this->remove_layer(tmp);
        delete tmp;
    }
    CNN_DEBUG_MSG("Conv_net is destroyed!");
}

int
Conv_net::append_layer(Layer *layer)
{
    CNN_DEBUG_MSG("Appending layer...");
    int ret = this->last_layer->connect_after(layer);
    if (ret == 0) {
        while (layer->next != nullptr) {
            layer = layer->next;
        }
        this->last_layer = layer;
    }
    return ret;
}

int
Conv_net::remove_layer(Layer *layer)
{
    CNN_DEBUG_MSG("Removing layer...");
    Layer *tmp = layer->prev;
    int ret = layer->disconnect();
    if (ret == 0 && this->last_layer == layer) {
        this->last_layer = tmp;
    }
    return ret;
}

int
Conv_net::feed_input(vector<vector<vector<outval_t>>> in)
{
    unsigned int depth = in.size();
    unsigned int height = 0;
    unsigned int width = 0;
    if (depth > 0) {
        height = in[0].size();
        if (height > 0) {
            width = in[0][0].size();
        }
        for (auto &a: in) {
            if (a.size() != height) {
                cerr << "Incompatible dimensions of input!" << endl;
                return 1;
            }
            for (auto &b: a) {
                if (b.size() != width) {
                    cerr << "Incompatible dimensions of input!" << endl;
                    return 1;
                }
            }
        }
    }
    if (depth == 0) {
        cerr << "Input depth must be greater than 0!" << endl;
        return 1;
    }
    if (height == 0) {
        cerr << "Input height must be greater than 0!" << endl;
        return 1;
    }
    if (width == 0) {
        cerr << "Input width must be greater than 0!" << endl;
        return 1;
    }
    if (depth != this->input_layer.dim_in.depth ||
            height != this->input_layer.dim_in.height ||
            width != this->input_layer.dim_in.width) {
        cerr << "Incompatible dimensions of input!" << endl;
        return 1;
    }
    this->input_layer.input = move(in);
    return 0;
}

void
Conv_net::feed_forward(void)
{
    this->input_layer.feed_forward();
}

void
Conv_net::backprop(vector<outval_t> expected_out)
{
    vector<vector<vector<double>>> deltas = vector<vector<vector<double>>>(
            this->last_layer->dim_out.depth, vector<vector<double>>(
            this->last_layer->dim_out.height, vector<double>(this->last_layer->dim_out.width)));
    unsigned int i = 0;
    for (unsigned int d = 0; d < this->last_layer->dim_out.depth; d++) {
        for (unsigned int h = 0; h < this->last_layer->dim_out.height; h++) {
            for (unsigned int w = 0; w < this->last_layer->dim_out.width; w++) {
                deltas[d][h][w] = expected_out[i] - this->last_layer->output[d][h][w];
                i++;
            }
        }
    }
    this->last_layer->backprop(this->mi, move(deltas));
}

void
Conv_net::set_mi(double _mi)
{
    this->mi = _mi;
}

void
Conv_net::print(ostream &stream)
{
    CNN_DEBUG_MSG("Printing Conv_net...");
    stream << "Learning rate: " << this->mi << endl;
    input_layer.print(stream);
    input_layer.print_output(stream);
    Layer *layer = &this->input_layer;
    while (layer->next != nullptr) {
        layer = layer->next;
        layer->print(stream);
        layer->print_output(stream);
    }
}
