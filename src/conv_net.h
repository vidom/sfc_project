/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef __CONV_NET_H__
#define __CONV_NET_H__

/* System and standard library headers. */
#include <iostream>
#include <vector>

/* Own headers. */
#include "layer.h"
#include "input_layer.h"

using namespace std;

/**
 * @brief Convolutional Neural Network
 */
class Conv_net {
public:
    Input_layer input_layer;
    Layer *last_layer;
    /* Learning rate */
    double mi;

    Conv_net(Dimensions dim_in, double _mi);
    ~Conv_net(void);

    int append_layer(Layer *layer);
    int remove_layer(Layer *layer);
    int feed_input(vector<vector<vector<outval_t>>> in);
    void feed_forward(void);
    void backprop(vector<outval_t> expected_out);
    void set_mi(double _mi);

    void print(ostream &stream);
};

#endif /* __CONV_NET_H__ */
