/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef __DEBUG_H_
#define __DEBUG_H_

#include <stdio.h>

#ifdef CNN_DEBUG
#define CNN_DEBUG_MSG(...) \
    do { \
        fprintf(stderr, "DEBUG|%s:% 4d:%s()| ", __FILE__, __LINE__, __func__); \
        fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, "\n"); \
    } while (0)
#define CNN_DEBUG_MSG_NN(...) \
    do { \
        fprintf(stderr, "DEBUG|%s:% 4d:%s()| ", __FILE__, __LINE__, __func__); \
        fprintf(stderr, __VA_ARGS__); \
    } while (0)
#define CNN_DEBUG_MSG_RAW(...) \
    do { \
        fprintf(stderr, __VA_ARGS__); \
    } while (0)
#else
#define CNN_DEBUG_MSG(...)
#define CNN_DEBUG_MSG_NN(...)
#define CNN_DEBUG_MSG_RAW(...)
#endif

#endif /* __DEBUG_H__ */
