/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <iostream>
#include <cstdlib>
#include <cmath>

/* Own headers. */
#include "debug.h"
#include "layer.h"
#include "fc_layer.h"

using namespace std;

Fc_layer::Fc_layer(Dimensions di, Dimensions dout, vector<vector<outval_t>> ws,
        vector<outval_t> bs, activation act):
    activation_func{act},
    base_output_sum{0}
{
    this->prev = nullptr;
    this->next = nullptr;
    this->dim_in = di;
    unsigned int weights_count = this->dim_in.depth * this->dim_in.height * this->dim_in.width;

    if (dout.width != 1 || dout.height != 1) {
        throw Layer_exception{"Wrong output dimensions!"};
    }

    if (ws.size() != dout.depth || bs.size() != dout.depth) {
        throw Layer_exception{"Wrong number of set of weights or biases!"};
    }

    for (auto &w: ws) {
        if (w.size() != weights_count) {
            throw Layer_exception{"Wrong number of weights!"};
        }
    }

    this->weights = ws;
    this->biases = bs;

    this->dim_out = dout;
    this->output = vector< vector< vector<outval_t> > >(
        this->dim_out.depth, vector< vector<outval_t> >(
            this->dim_out.height, vector<outval_t>(this->dim_out.width)));

    CNN_DEBUG_MSG("Fc_layer is created!");
}

Fc_layer::Fc_layer(Dimensions di, Dimensions dout, activation act):
    activation_func{act},
    base_output_sum{0}
{
    this->prev = nullptr;
    this->next = nullptr;
    this->dim_in = di;
    unsigned int weights_count = this->dim_in.depth * this->dim_in.height * this->dim_in.width;

    if (dout.width != 1 || dout.height != 1) {
        throw Layer_exception{"Wrong output dimensions!"};
    }

    srand(4242);

    this->weights = vector<vector<outval_t>>(dout.depth, vector<outval_t>(weights_count));
    this->biases = vector<outval_t>(dout.depth);

    for (auto &ws: this->weights) {
        for (auto &w: ws) {
            w = ((outval_t)rand()) / (RAND_MAX);
        }
    }

    for (auto &b: this->biases) {
        b = ((outval_t)rand()) / (RAND_MAX);
    }

    this->dim_out = dout;
    this->output = vector< vector< vector<outval_t> > >(
        this->dim_out.depth, vector< vector<outval_t> >(
            this->dim_out.height, vector<outval_t>(this->dim_out.width)));

    CNN_DEBUG_MSG("Fc_layer is created!");
}

Fc_layer::~Fc_layer(void)
{
    CNN_DEBUG_MSG("Fc_layer is destroyed!");
}

outval_t
Fc_layer::get_input(unsigned int weight_num)
{
    unsigned int depth = weight_num / this->dim_in.depth;
    unsigned int height = (weight_num % this->dim_in.depth) / this->dim_in.height;
    unsigned int width = (weight_num % this->dim_in.depth) % this->dim_in.height;
    return this->prev->output[depth][height][width];
}

void
Fc_layer::pre_feed_forward(void)
{
    this->base_output_sum = 0;
}

void
Fc_layer::post_feed_forward(void)
{
    if (this->activation_func == SOFTMAX) {
        for (auto &d: this->output) {
            for (auto &h: d) {
                for (auto &w: h) {
                    w = exp(w) / this->base_output_sum;
                }
            }
        }
    }
}

outval_t
Fc_layer::get_neuron_output(unsigned int d,
        unsigned int h, unsigned int w)
{
    outval_t out_value = 0;
    unsigned int i = 0;
    (void)h;
    (void)w;
    for (auto &depth: this->prev->output) {
        for (auto &height: depth) {
            for (auto &width: height) {
                out_value += width * this->weights[d][i];
                i++;
            }
        }
    }
    out_value += this->biases[d];
    this->base_output_sum += exp(out_value);
    if (this->activation_func == RELU) {
        out_value = (out_value < 0) ? 0 : out_value;
    }
    return out_value;
}

void
Fc_layer::backprop(double mi, vector<vector<vector<double> > > deltas)
{
    if (this->next != nullptr) {
        throw Layer_exception{"Unsupported CNN architecture for backpropagation!"};
    }
    vector<vector<vector<double>>> new_deltas = vector<vector<vector<double>>>(
            this->prev->dim_out.depth, vector<vector<double>>(this->prev->dim_out.height,
            vector<double>(this->prev->dim_out.width, 0)));
    /* Count deltas for previous layer. */
    for (unsigned int d = 0; d < this->dim_out.depth; d++) {
        for (unsigned int h = 0; h < this->dim_out.height; h++) {
            for (unsigned int w = 0; w < this->dim_out.width; w++) {
                unsigned int i = 0;
                for (unsigned int dp = 0; dp < this->prev->dim_out.depth; dp++) {
                    for (unsigned int hp = 0; hp < this->prev->dim_out.height; hp++) {
                        for (unsigned int wp = 0; wp < this->prev->dim_out.width; wp++) {
                            new_deltas[dp][hp][wp] = new_deltas[dp][hp][wp] + deltas[d][h][w] *
                                    this->weights[d][i];
                            i++;
                        }
                    }
                }
            }
        }
    }
    /* Count new weights in this layer. */
    for (unsigned int d = 0; d < this->dim_out.depth; d++) {
        for (unsigned int h = 0; h < this->dim_out.height; h++) {
            for (unsigned int w = 0; w < this->dim_out.width; w++) {
                unsigned int i = 0;
                for (auto &ditem: this->prev->output) {
                    for (auto &hitem: ditem) {
                        for (auto &witem: hitem) {
                            this->weights[d][i] = this->weights[d][i] + mi * deltas[d][h][w] * witem;
                            i++;
                        }
                    }
                }
                this->biases[d] = this->biases[d] + mi * deltas[d][h][w];
            }
        }
    }
    this->prev->backprop(mi, new_deltas);
}

void
Fc_layer::print(ostream &stream)
{
    stream << "--Fully-connected Layer--" << endl;
    this->print_dim(stream);
}
