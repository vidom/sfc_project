/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef __FC_LAYER_H__
#define __FC_LAYER_H__

/* System and standard library headers. */
#include <iostream>
#include <vector>

/* Own headers. */
#include "layer.h"

using namespace std;

/**
 * @brief Fully-connected Layer
 */
class Fc_layer: public Layer {
public:
    enum activation {
        RELU,
        SOFTMAX
    };

    activation activation_func;
    double base_output_sum;
    vector<vector<outval_t>> weights;
    vector<outval_t> biases;

    Fc_layer(Dimensions di, Dimensions dout, vector<vector<outval_t>> ws,
            vector<outval_t> bs, activation act);
    Fc_layer(Dimensions di, Dimensions dout, activation act);
    ~Fc_layer(void);

    outval_t get_input(unsigned int weight_num);
    void pre_feed_forward(void);
    void post_feed_forward(void);
    outval_t get_neuron_output(unsigned int d, unsigned int h, unsigned int w);
    void backprop(double mi, vector<vector<vector<double>>> deltas);

    void print(ostream &stream);
};

#endif /* __FC_LAYER_H__ */
