/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <iostream>

/* Own headers. */
#include "debug.h"
#include "layer.h"
#include "input_layer.h"

using namespace std;

Input_layer::Input_layer(Dimensions di)
{
    this->prev = nullptr;
    this->next = nullptr;
    this->dim_in = di;
    this->dim_out = di;
    this->output = vector< vector< vector<outval_t> > >(
        di.depth, vector< vector<outval_t> >(di.height, vector<outval_t>(di.width)));

    CNN_DEBUG_MSG("Input_layer is created!");
}

Input_layer::~Input_layer(void)
{
    CNN_DEBUG_MSG("Input_layer is destroyed!");
}

int
Input_layer::connect_before(Layer *layer)
{
    CNN_DEBUG_MSG("Connecting before input layer...");
    (void)layer;
    cerr << "Cannot connect layer before input layer!" << endl;
    return 1;
}

int
Input_layer::disconnect(void)
{
    CNN_DEBUG_MSG("Disconnecting input layer...");
    cerr << "Cannot disconnect input layer!" << endl;
    return 1;
}

outval_t
Input_layer::get_neuron_output(unsigned int d,
        unsigned int h, unsigned int w)
{
    return this->input[d][h][w];
}

void
Input_layer::backprop(double mi, vector<vector<vector<double> > > deltas)
{
    (void)mi;
    (void)deltas;
}

void
Input_layer::print(ostream &stream)
{
    stream << "--Input Layer--" << endl;
    this->print_dim(stream);
}
