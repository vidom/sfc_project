/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef __INPUT_LAYER_H__
#define __INPUT_LAYER_H__

/* System and standard library headers. */
#include <iostream>
#include <vector>

/* Own headers. */
#include "layer.h"

using namespace std;

/**
 * @brief Input Layer
 */
class Input_layer: public Layer {
public:
    vector<vector<vector<outval_t>>> input;

    Input_layer(Dimensions di);
    ~Input_layer(void);

    int connect_before(Layer *layer);
    int disconnect(void);
    outval_t get_neuron_output(unsigned int d, unsigned int h, unsigned int w);
    void backprop(double mi, vector<vector<vector<double>>> deltas);

    void print(ostream &stream);
};

#endif /* __INPUT_LAYER_H__ */
