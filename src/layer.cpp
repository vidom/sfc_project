/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <iostream>
#include <vector>

/* Own headers. */
#include "debug.h"
#include "layer.h"

using namespace std;

Layer::Layer(void) :
    prev{nullptr},
    next{nullptr}
{
    CNN_DEBUG_MSG("Layer is created!");
}

Layer::~Layer(void)
{
    CNN_DEBUG_MSG("Layer is destroyed!");
}

bool
Layer::in_is_compatible(Layer *layer)
{
    return this->dim_in == layer->dim_out;
}

bool
Layer::out_is_compatible(Layer *layer)
{
    return this->dim_out == layer->dim_in;
}

int
Layer::connect_before(Layer *layer)
{
    CNN_DEBUG_MSG("Connecting before layer...");
    Layer *first = layer;
    Layer *last = layer;
    while (first->prev != nullptr) {
        first = first->prev;
    }
    while (last->next != nullptr) {
        last = last->next;
    }

    if (this->prev == nullptr) {
        if (this->in_is_compatible(last)) {
            last->next = this;
            last->next->prev = last;
        } else {
            cerr << "Layers are not compatible!" << endl;
            return 1;
        }
    } else {
        if (this->in_is_compatible(last) && this->prev->out_is_compatible(first)) {
            first->prev = this->prev;
            first->prev->next = first;
            last->next = this;
            last->next->prev = last;
        } else {
            cerr << "Layers are not compatible!" << endl;
            return 1;
        }
    }

    return 0;
}

int
Layer::connect_after(Layer *layer)
{
    CNN_DEBUG_MSG("Connecting after layer...");
    Layer *first = layer;
    Layer *last = layer;
    while (first->prev != nullptr) {
        first = first->prev;
    }
    while (last->next != nullptr) {
        last = last->next;
    }

    if (this->next == nullptr) {
        if (this->out_is_compatible(first)) {
            first->prev = this;
            first->prev->next = first;
        } else {
            cerr << "Layers are not compatible!" << endl;
            return 1;
        }
    } else {
        if (this->out_is_compatible(first) && this->next->in_is_compatible(last)) {
            last->next = this->next;
            last->next->prev = last;
            first->prev = this;
            first->prev->next = first;
        } else {
            cerr << "Layers are not compatible!" << endl;
            return 1;
        }
    }

    return 0;
}

int
Layer::disconnect(void)
{
    CNN_DEBUG_MSG("Disconnecting layer...");
    if (this->prev != nullptr && this->next != nullptr) {
        if (this->prev->out_is_compatible(this->next)) {
            this->prev->next = this->next;
            this->next->prev = this->prev;
        } else {
            cerr << "Cannot disconnect layer because adjacent layers are not compatible!" << endl;
            return 1;
        }
    } else if (this->next != nullptr) {
        this->next->prev = nullptr;
    } else if (this->prev != nullptr) {
        this->prev->next = nullptr;
    }
    this->prev = nullptr;
    this->next = nullptr;
    return 0;
}

void
Layer::pre_feed_forward(void)
{
}

void
Layer::post_feed_forward(void)
{
}

void
Layer::feed_forward(void)
{
    unsigned int d;
    unsigned int h;
    unsigned int w;
    this->pre_feed_forward();
    for (d = 0; d < this->dim_out.depth; d++) {
        for (h = 0; h < this->dim_out.height; h++) {
            for (w = 0; w < this->dim_out.width; w++) {
                this->output[d][h][w] = this->get_neuron_output(d, h, w);
            }
        }
    }
    this->post_feed_forward();
    if (this->next != nullptr) {
        this->next->feed_forward();
    }
}

void
Layer::print_dim(ostream &stream)
{
    stream << "  Input dimensions (depth x height x width):" << endl;
    stream << "    " << this->dim_in.depth << " x " <<
        this->dim_in.height << " x " << this->dim_in.width << endl;
    stream << "  Output dimensions (depth x height x width):" << endl;
    stream << "    " << this->dim_out.depth << " x " <<
        this->dim_out.height << " x " << this->dim_out.width << endl;
}

void
Layer::print_output(ostream &stream)
{
    unsigned int d;
    unsigned int h;
    unsigned int w;
    for (d = 0; d < this->output.size(); d++) {
        for (h = 0; h < this->output[d].size(); h++) {
            for (w = 0; w < this->output[d][h].size(); w++) {
                stream << this->output[d][h][w];
                if (w < this->output[d][h].size() - 1) {
                    stream << " ";
                }
            }
            stream << endl;
        }
        if (d < this->output.size() - 1) {
            stream << endl;
        }
    }
}
