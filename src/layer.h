/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef __LAYERS_H__
#define __LAYERS_H__

/* System and standard library headers. */
#include <iostream>
#include <vector>
#include <exception>
#include <string>

using namespace std;

struct Dimensions {
    unsigned int depth;
    unsigned int height;
    unsigned int width;
};

inline bool operator==(const Dimensions& lhs, const Dimensions& rhs)
{
    return lhs.width == rhs.width &&
        lhs.height == rhs.height &&
        lhs.depth == rhs.depth;
}

inline bool operator!=(const Dimensions& lhs, const Dimensions& rhs)
{
    return !(lhs == rhs);
}

typedef double outval_t;

struct Layer_exception : exception {
    string msg;
    Layer_exception(string m) : msg{m} {}
    const char *what() const noexcept
    {
        return string{string{"Layer_exception: "} + msg}.c_str();
    }
};

/**
 * @brief Base class for all layers.
 */
class Layer {
public:
    Layer *prev;
    Layer *next;
    Dimensions dim_in;
    Dimensions dim_out;
    /* output[depth][height][width] */
    vector<vector<vector<outval_t>>> output;

    Layer(void);
    virtual ~Layer(void);

    virtual bool in_is_compatible(Layer *layer);
    virtual bool out_is_compatible(Layer *layer);
    virtual int connect_before(Layer *layer);
    virtual int connect_after(Layer *layer);
    virtual int disconnect(void);
    virtual void pre_feed_forward(void);
    virtual void post_feed_forward(void);
    virtual void feed_forward(void);
    virtual outval_t get_neuron_output(unsigned int d, unsigned int h, unsigned int w) =0;
    virtual void backprop(double mi, vector<vector<vector<double>>> deltas) =0;

    virtual void print(ostream &stream) =0;
    virtual void print_dim(ostream &stream);
    virtual void print_output(ostream &stream);
};

#endif /* __LAYERS_H__*/
