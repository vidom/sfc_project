/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <iostream>
#include <stdlib.h>

/* Own headers. */
#include "debug.h"
#include "layers.h"
#include "conv_net.h"
#include "conv_layer.h"
#include "activation_layer.h"
#include "fc_layer.h"
#include "pooling_layer.h"

using namespace std;

int main(void)
{
    int ret;
    Conv_net conv_net{Dimensions{3, 5, 5}};

    vector<outval_t> v1 = {0,-1,-1};
    vector<outval_t> v2 = {-1,-1,1};
    vector<outval_t> v3 = {-1,-1,0};
    vector<outval_t> v4 = {1,0,0};
    vector<outval_t> v5 = {1,1,0};
    vector<outval_t> v6 = {-1,-1,1};
    vector<outval_t> v7 = {0,0,-1};
    vector<outval_t> v8 = {1,0,-1};
    vector<outval_t> v9 = {1,-1,-1};

    vector<outval_t> v11 = {0,-1,0};
    vector<outval_t> v12 = {-1,1,1};
    vector<outval_t> v13 = {-1,1,-1};
    vector<outval_t> v14 = {-1,-1,1};
    vector<outval_t> v15 = {1,-1,0};
    vector<outval_t> v16 = {-1,0,0};
    vector<outval_t> v17 = {1,1,1};
    vector<outval_t> v18 = {0,0,-1};
    vector<outval_t> v19 = {1,0,1};

    vector<vector<outval_t>> m1 = {v1, v2, v3};
    vector<vector<outval_t>> m2 = {v4, v5, v6};
    vector<vector<outval_t>> m3 = {v7, v8, v9};

    vector<vector<outval_t>> m4 = {v11, v12, v13};
    vector<vector<outval_t>> m5 = {v14, v15, v16};
    vector<vector<outval_t>> m6 = {v17, v18, v19};

    vector<vector<vector<outval_t>>> f1 = {m1,m2,m3};
    vector<vector<vector<outval_t>>> f2 = {m4,m5,m6};

    vector<vector<vector<vector<outval_t>>>> filters = {f1, f2};
    vector<outval_t> biases = {1, 0};

    Conv_layer cl{Dimensions{3,5,5}, 2, 3, 3, 2, 1, 0};
    ret = conv_net.append_layer(&cl);
    if (ret != 0) {
        cerr << "Error: finish!" << endl;
        return EXIT_FAILURE;
    }

    Activation_layer al{Dimensions{2, 3, 3}};
    ret = conv_net.append_layer(&al);
    if (ret != 0) {
        cerr << "Error: finish!" << endl;
        return EXIT_FAILURE;
    }

    Pooling_layer pl{Dimensions{2, 3, 3}, 2, 2, 1, Pooling_layer::MAX};
    ret = conv_net.append_layer(&pl);
    if (ret != 0) {
        cerr << "Error: finish!" << endl;
        return EXIT_FAILURE;
    }

    Fc_layer fl{Dimensions{2, 2, 2}, Dimensions{10, 1, 1}, Fc_layer::SOFTMAX};
    ret = conv_net.append_layer(&fl);
    if (ret != 0) {
        cerr << "Error: finish!" << endl;
        return EXIT_FAILURE;
    }

    conv_net.print(cout);

    vector<outval_t> i1 = {0,2,1,2,2};
    vector<outval_t> i2 = {1,1,0,1,0};
    vector<outval_t> i3 = {2,1,1,0,1};
    vector<outval_t> i4 = {2,1,1,1,2};
    vector<outval_t> i5 = {0,0,0,2,0};

    vector<outval_t> i11 = {0,1,0,0,1};
    vector<outval_t> i12 = {0,2,0,0,2};
    vector<outval_t> i13 = {2,2,2,1,2};
    vector<outval_t> i14 = {0,2,1,2,0};
    vector<outval_t> i15 = {0,2,0,1,0};

    vector<outval_t> i21 = {2,1,1,0,0};
    vector<outval_t> i22 = {1,1,1,1,2};
    vector<outval_t> i23 = {0,2,1,0,2};
    vector<outval_t> i24 = {0,0,1,2,1};
    vector<outval_t> i25 = {1,2,0,1,0};

    vector<vector<outval_t>> matrix1 = {i1,i2,i3,i4,i5};
    vector<vector<outval_t>> matrix2 = {i11,i12,i13,i14,i15};
    vector<vector<outval_t>> matrix3 = {i21,i22,i23,i24,i25};
    vector<vector<vector<outval_t>>> input_values{matrix1,matrix2,matrix3};

    ret = conv_net.feed_input(input_values);
    if (ret != 0) {
        cerr << "Error: finish!" << endl;
        return EXIT_FAILURE;
    }

    conv_net.feed_forward();
    conv_net.print(cout);

    return EXIT_SUCCESS;
}
