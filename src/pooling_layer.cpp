/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

/* System and standard library headers. */
#include <iostream>

/* Own headers. */
#include "debug.h"
#include "layer.h"
#include "pooling_layer.h"

using namespace std;

Pooling_layer::Pooling_layer(Dimensions di, unsigned int fh,
        unsigned int fw, unsigned int s, operation_type ot):
    filter_height{fh},
    filter_width{fw},
    stride{s},
    operation{ot}
{
    this->prev = nullptr;
    this->next = nullptr;
    this->dim_in = di;

    if (di.width < fw || (di.width - fw) % s != 0 ||
            di.height < fh || (di.height - fh) % s != 0 ||
            fh == 0 || fw == 0 || s == 0) {
        throw Layer_exception{"Incompatible parameters for pooling layer!"};
    }

    this->dim_out.depth = di.depth;
    this->dim_out.height = ((di.height - fh) / s) + 1;
    this->dim_out.width = ((di.width - fw) / s) + 1;
    this->output = vector< vector< vector<outval_t> > >(
        this->dim_out.depth, vector< vector<outval_t> >(
            this->dim_out.height, vector<outval_t>(this->dim_out.width)));

    CNN_DEBUG_MSG("Pooling_layer is created!");
}

Pooling_layer::~Pooling_layer(void)
{
    CNN_DEBUG_MSG("Pooling_layer is destroyed!");
}

outval_t
Pooling_layer::get_neuron_output(unsigned int d,
        unsigned int h, unsigned int w)
{
    outval_t out_value;
    if (this->operation == AVERAGE) {
        /* Average Pooling */
        unsigned int r;
        unsigned int c;
        outval_t sum = 0;
        outval_t count = 0;
        for (c = 0; c < this->filter_height; c++) {
            for (r = 0; r < this->filter_width; r++) {
                sum += this->prev->output[d][(h * this->stride) + c][(w * this->stride) + r];
                count += 1;
            }
        }
        out_value = sum / count;
    } else {
        /* Max Pooling */
        bool first = true;
        unsigned int r;
        unsigned int c;
        outval_t max = 0;
        for (c = 0; c < this->filter_height; c++) {
            for (r = 0; r < this->filter_width; r++) {
                if (first) {
                    max = this->prev->output[d][(h * this->stride) + c][(w * this->stride) + r];
                    first = false;
                } else if (this->prev->output[d][(h * this->stride) + c][(w * this->stride) + r] > max) {
                    max = this->prev->output[d][(h * this->stride) + c][(w * this->stride) + r];
                }
            }
        }
        out_value = max;
    }
    return out_value;
}

void
Pooling_layer::backprop(double mi, vector<vector<vector<double> > > deltas)
{
    vector<vector<vector<double>>> new_deltas = vector<vector<vector<double>>>(
            this->prev->dim_out.depth, vector<vector<double>>(this->prev->dim_out.height,
            vector<double>(this->prev->dim_out.width, 0)));
    /* Count deltas for previous layer. */
    if (this->operation == AVERAGE) {
        /* Average Pooling */
        for (unsigned int d = 0; d < this->dim_out.depth; d++) {
            for (unsigned int h = 0; h < this->dim_out.height; h++) {
                for (unsigned int w = 0; w < this->dim_out.width; w++) {
                    for (unsigned int r = 0; r < this->filter_height; r++) {
                        for (unsigned int c = 0; c < this->filter_width; c++) {
                            new_deltas[d][(h * this->stride) + r][(w * this->stride) + c] =
                                    new_deltas[d][(h * this->stride) + r][(w * this->stride) + c] +
                                    deltas[d][h][w] / (this->filter_height * this->filter_width);
                        }
                    }
                }
            }
        }
    } else {
        /* Max Pooling */
        for (unsigned int d = 0; d < this->dim_out.depth; d++) {
            for (unsigned int h = 0; h < this->dim_out.height; h++) {
                for (unsigned int w = 0; w < this->dim_out.width; w++) {
                    bool first = true;
                    unsigned int r_max = 0;
                    unsigned int c_max = 0;
                    outval_t max = 0;
                    for (unsigned int r = 0; r < this->filter_height; r++) {
                        for (unsigned int c = 0; c < this->filter_width; c++) {
                            if (first) {
                                r_max = h * this->stride + r;
                                c_max = w * this->stride + c;
                                max = this->prev->output[d][r_max][c_max];
                                first = false;
                            } else if (this->prev->output[d][(h * this->stride) + r][(w * this->stride) + c] > max) {
                                r_max = h * this->stride + r;
                                c_max = w * this->stride + c;
                                max = this->prev->output[d][r_max][c_max];
                            }
                        }
                    }
                    new_deltas[d][r_max][c_max] = new_deltas[d][r_max][c_max] + deltas[d][h][w];
                }
            }
        }
    }
    this->prev->backprop(mi, new_deltas);
}

void
Pooling_layer::print(ostream &stream)
{
    stream << "--Pooling Layer--" << endl;
    this->print_dim(stream);
}
