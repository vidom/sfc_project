/*
 * @brief  SFC 2017/2018 Demonstration of Convolutional Neural Network activity
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date   2017/11
 */

#ifndef __POOLING_LAYER_H__
#define __POOLING_LAYER_H__

/* System and standard library headers. */
#include <iostream>
#include <vector>

/* Own headers. */
#include "layer.h"

using namespace std;

/**
 * @brief Pooling Layer
 */
class Pooling_layer: public Layer {
public:
    enum operation_type {
        MAX,
        AVERAGE
    };

    unsigned int filter_height;
    unsigned int filter_width;
    unsigned int stride;
    operation_type operation;

    Pooling_layer(Dimensions di, unsigned int fh, unsigned int fw,
            unsigned int s, operation_type ot);
    ~Pooling_layer(void);

    outval_t get_neuron_output(unsigned int d, unsigned int h, unsigned int w);
    void backprop(double mi, vector<vector<vector<double>>> deltas);

    void print(ostream &stream);
};

#endif /* __POOLING_LAYER_H__ */
